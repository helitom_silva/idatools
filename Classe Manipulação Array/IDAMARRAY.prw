#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'IDATOOLS.CH'

#DEFINE CR Chr(13) + Chr(10)


/*/{Protheus.doc} IdaMArray
@description Classe para manipulacao de Array

@author  Helitom Silva
@since   05/02/2016
@version 1.0

@obs Formato do Array aLista{aItens{acampos[nField][nValue]}}

/*/
Class IdaMArray

    Data aLista
    Data aItem

    Data nPosField
    Data nPosValue

    Method New(p_aLista) Constructor
    Method GoItem(p_nItem)
    Method GetValue(p_uField)
    Method SetValue(p_uField, p_uValue)
    Method GetQttItens()
    Method GetLista()

EndClass


/*/{Protheus.doc} New
@description Construtor da Classe IdaMArray.

@author  Helitom Silva
@since   05/02/2016
@version 1.0

@param p_aLista, Array, Array no formato aLista{aItens{acampos[nField][nValue]}}

/*/
Method New(p_aLista) Class IdaMArray

    Default p_aLista := {}

    ::nPosField := 1
    ::nPosValue := 2

    ::aLista := {}
    ::aItem  := {}

    ::aLista := p_aLista

    If Len(::alista) > 0
        ::GoItem(1)
    EndIf

Return Self


/*/{Protheus.doc} GoItem
@description Posiciona no Item do Array.

@author  Helitom Silva
@since   05/02/2016
@version 1.0

@param p_nItem, Numerico, Numero do Item

/*/
Method GoItem(p_nItem) Class IdaMArray

    Default p_nItem := 1

    If p_nItem > 0
        ::aItem := ::aLista[p_nItem]
    EndIF

Return


/*/{Protheus.doc} GetValue
@description Obtem Valor do Campo do Item.

@author  Helitom Silva
@since   05/02/2016
@version 1.0

@param p_uField, Numerico ou Caracter, Posicao do Campo ou Nome do Campo
@param uRet, Indefinido, Valor do Campo

/*/
Method GetValue(p_uField) Class IdaMArray

    Local uRet 	 := Nil
    Local nField := 0

    If ValType(p_uField) = 'N'
        uRet := ::aItem[p_uField][::nPosValue]
    ElseIf ValType(p_uField) = 'C'

        If (nField := aScan(::aItem, {|X| X[::nPosField] == p_uField})) > 0
            uRet := ::aItem[nField][::nPosValue]
        Else
            MsgInfo('Campo n�o encontrado na estutura do ' + p_uField + ' Array!', 'Classe: IdaMArray')
        EndIf

    EndIf

Return uRet


/*/{Protheus.doc} SetValue
@description Define Valor do Campo do Item.

@author  Helitom Silva
@since   05/02/2016
@version 1.0

@param p_uField, Numerico ou Caracter, Posicao do Campo ou Nome do Campo
@param p_uValue, Indefinido, Valor do Campo

/*/
Method SetValue(p_uField, p_uValue) Class IdaMArray

    Local nField := 0

    If ValType(p_uField) = 'N'
        ::aItem[p_uField][::nPosValue] := p_uValue
    ElseIf ValType(p_uField) = 'C'

        If (nField := aScan(::aItem, {|X| X[::nPosField] == p_uField})) > 0
            ::aItem[nField][::nPosValue] := p_uValue
        Else
            MsgInfo('Campo n�o encontrado na estutura do ' + p_uField + ' Array!', 'Classe: IdaMArray')
        EndIf

    EndIf

Return

/*/{Protheus.doc} GetQttItens
@description Define Valor do Campo do Item.

@author  Helitom Silva
@since   05/02/2016
@version 1.0

@param p_uField, Numerico ou Caracter, Posicao do Campo ou Nome do Campo
@param p_uValue, Indefinido, Valor do Campo

/*/
Method GetQttItens() Class IdaMArray

    Local nRet := Len(::aLista)

Return nRet


/*/{Protheus.doc} GetLista
@description Obtem Lista manipulada pela classe

@author  Helitom Silva
@since   28/04/2016
@version 1.0

@return aRet, Array, Lista manipulada pela Classe

/*/
Method GetLista() Class IdaMArray

    Local aRet := Clone(::aLista)

Return aRet