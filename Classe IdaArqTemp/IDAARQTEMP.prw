#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'IDATOOLS.CH'


/*/{Protheus.doc} IdaArqTemp
@description Classe para Manipula��o de Arquivos Tempor�rios

@author Helitom Silva
@since  14/04/2015

/*/
Class IdaArqTemp

    DATA cAliasTmp
    DATA cDirTemp
    DATA cArqTemp

    METHOD New() CONSTRUCTOR
    METHOD CreateAlias() CONSTRUCTOR
    METHOD GetAlias()
    METHOD DeleteAlias()

EndClass


/*/{Protheus.doc} NEW
@description Cria New

@author Helitom Silva
@since  14/04/2015

/*/
METHOD New() CLASS IdaArqTemp

    Self:cAliasTmp := Self:GetAlias()
    Self:cDirTemp  := "\Temp\"
    Self:cArqTemp  := ""

RETURN Self


/*/{Protheus.doc} CreateAlias
@description Cria Alias temporario

@author Helitom Silva
@since  14/04/2015

/*/
METHOD CreateAlias(p_aStruct) CLASS IdaArqTemp

    CloseAlias(Self:GetAlias())

    Self:cArqTemp := CriaTrab(p_aStruct)
    DbUseArea( .T.,, Self:cArqTemp, Self:GetAlias(), .T., .F. )

RETURN Self


/*/{Protheus.doc} GetAlias
@description Deleta Alias temporario

@author Helitom Silva
@since  14/04/2015

/*/
METHOD GetAlias() CLASS IdaArqTemp

    If Empty(Self:cAliasTmp)

        Self:cAliasTmp := GetNextAlias()

    EndIf

RETURN Self:cAliasTmp


/*/{Protheus.doc} DeleteAlias
@description Deleta Alias temporario

@author Helitom Silva
@since  14/04/2015

/*/
METHOD DeleteAlias() CLASS IdaArqTemp

    If Select(Self:GetAlias()) > 0

        DbSelectArea(Self:GetAlias())
        (Self:GetAlias())->(DbCloseArea())

        If .not. (FErase(Self:cArqTemp + GetDbExtension()) <> -1)
            MsgAlert('Erro na elimina��o do arquivo n� ' + Str(FError()))
        EndIf

    EndIf

RETURN