#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TOTVS.CH'

/*/{Protheus.doc} IdaDialog
@description Classe de Dialogo Ida Inform�tica.
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Class IdaDialog From MSDialog

    Data cOrigem HIDDEN
    Data cIdentity HIDDEN

    Method New() CONSTRUCTOR
    Method Close()
    Method Activate()
    Method SetOrigem()
    Method GetOrigem()
    Method SetIdentity()
    Method GetIdentity()
    Method SearchOrigem()
    Method SetKey()

EndClass


/*/{Protheus.doc} New
@description Inicia Objeto IdaDialog - (Construtor)
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method New(  nTop,  nLeft,  nBottom,  nRight,  cCaption,  uParam6,  uParam7,  uParam8,  uParam9,  nClrText,  nClrBack,  uParam12,  oWnd,  lPixel,  uParam15,  uParam16,  uParam17,  lTransparent ) CLASS IdaDialog
    :New(  nTop,  nLeft,  nBottom,  nRight,  cCaption,  uParam6,  uParam7,  uParam8,  uParam9,  nClrText,  nClrBack,  uParam12,  oWnd,  lPixel,  uParam15,  uParam16,  uParam17,  lTransparent )

    Self:SearchOrigem()

Return Self


/*/{Protheus.doc} Activate
@description Ativa e exibe Formul�rio
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method Activate(uParam1,  uParam2,  uParam3,  lCentered,  bValid,  uParam6,  bInit,  uParam8,  uParam9) Class IdaDialog

    Self:SetKey(,,3) /* Inibe teclas de atalhos j� definidas */

    SetKey(K_CTRL_0, {|| oForm := GETWNDDEFAULT(), Iif(Upper(oForm:ClassName()) == 'IDADIALOG', MsgInfo('Formul�rio criado na Fun��o: ' + oForm:GetOrigem() , 'Dados do Formul�rio'), MsgInfo('N�o encontrado fun��o de Origem do Formul�rio.'))})

    _Super:Activate(  uParam1,  uParam2,  uParam3,  lCentered,  {|| Iif(ValType(bValid) == 'B', Iif( Eval(bValid), Self:SetKey(,,2), .F.), Self:SetKey(,,2))},  uParam6,  bInit,  uParam8,  uParam9 )

Return


/*/{Protheus.doc} SetOrigem
@description Define Origem
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method SetOrigem(p_cOrigem) Class IdaDialog

    Self:cOrigem := p_cOrigem

Return


/*/{Protheus.doc} GetOrigem
@description Obtem Origem
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method GetOrigem() Class IdaDialog
Return Self:cOrigem


/*/{Protheus.doc} SetIdentity
@description Define Identidade
	
@author  Helitom Silva
@since   12/07/2015
@version 1.0		

/*/
Method SetIdentity(p_cIdentity) Class IdaDialog

    Self:cIdentity := p_cIdentity

Return


/*/{Protheus.doc} GetIdentity
@description Obtem Identidade
	
@author  Helitom Silva
@since   12/07/2015
@version 1.0		

/*/
Method GetIdentity() Class IdaDialog
Return Self:cIdentity


/*/{Protheus.doc} SearchOrigem
@description Pesquisa Origem
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method SearchOrigem() Class IdaDialog

    Local nX     := 1
    Local cVazio := AllTrim(ProcName(nX))

    While !Empty(cVazio)
        If SubStr(Upper(AllTrim(ProcName(nX))), 1, 2) = 'U_'
            Self:SetOrigem(AllTrim(ProcName(nX)))
            Self:SetIdentity(Self:GetOrigem() + cValToChar(Self:HWnd))
            Exit
        Else
            nX++
            cVazio := AllTrim(ProcName(nX))
        EndIf
    End

Return


/*/{Protheus.doc} SetKey
@description Define, guarda e restaura atalhos
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method SetKey(p_uTecla, p_bBloco, p_nTipo) Class IdaDialog

    Local cProcAnt   := ''
    Local nX 		 := 0

    Default p_nTipo  := 1

	/*--------------------------|p_nTipo|--------------------------
		1 - Guarda/Seta os atalhos usados na Rotina
		2 - Restaura os atalhos Guardados
		3 - Inibe teclas de atalhos j� definidas
		4 - Limpa historico de defini��o Teclas de atalhos
	*/

    If Type('aIdaSetKey') == 'U'
        Public aIdaSetKey  := {}
    EndIf

    Do Case

    Case p_nTipo = 1

        SetKey(p_uTecla, p_bBloco)
        aAdd(aIdaSetKey, {p_uTecla, p_bBloco, Self:GetIdentity()})

    Case p_nTipo = 2

        If Len(aIdaSetKey) > 0

            For nX := Len(aIdaSetKey) to 1 step - 1

                If aIdaSetKey[nX][3] == Self:GetIdentity()

                    SetKey(aIdaSetKey[nX][1], Nil)

                    aDel(aIdaSetKey, nX)
                    aSize(aIdaSetKey, nX - 1)

                    Loop

                EndIf

                If Empty(AllTrim(cProcAnt)) .and. !(Self:GetIdentity() == aIdaSetKey[nX][3])
                    cProcAnt := aIdaSetKey[nX][3]
                EndIf

                If (aIdaSetKey[nX][3] == cProcAnt)

                    SetKey(aIdaSetKey[nX][1], Nil)

                    If ValType(aIdaSetKey[nX][2]) == 'B'
                        SetKey(aIdaSetKey[nX][1], aIdaSetKey[nX][2])
                    EndIf

                EndIf

            Next

        EndIf

    Case p_nTipo = 3

        If Len(aIdaSetKey) > 0

            For nX := Len(aIdaSetKey) to 1 step - 1

                If Empty(AllTrim(cProcAnt)) .and. !(Self:GetIdentity() == aIdaSetKey[nX][3])
                    cProcAnt := aIdaSetKey[nX][3]
                EndIf

                If (aIdaSetKey[nX][3] == cProcAnt)

                    SetKey(aIdaSetKey[nX][1], Nil)

                EndIf

            Next

        EndIf

			/* Defini��o da tecla de atalho F24 com bloco Nil, para que na restaura��o volte apenas um Nivel de Formul�rio */
        Self:SetKey(135, Nil)

    Case p_nTipo = 4

        aIdaSetKey := Nil

    EndCase

Return


/*/{Protheus.doc} Close
@description Fecha Dialogo

@author  Helitom Silva
@since   12/07/2015
@version 1.0

/*/
Method Close() class IdaDialog

    Self:SetKey(,,2)

    _Super:End()

Return