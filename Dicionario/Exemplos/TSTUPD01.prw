#include "totvs.ch"
#include "protheus.ch"

/*/{Protheus.doc} TSTUPD01

Atualiza dicionario

@author  Fernando Alencar
@version P11 e P10
@since   13/11/2011
@obs

/*/
User Function TSTUPD01()

    Default oProcess  := Nil
    Default cEmpresa  := "XX"

    oProcess:SetRegua2( 10 )

    oProcess:IncRegua2( 'Atualizando SX1')
    ExecSX1()

    oProcess:IncRegua2( 'Atualizando SX3')
    ExecSX3()

//bug - para funcionar corretamente, �
//necess�rio executar o sx3 antes do sx2
    oProcess:IncRegua2( 'Atualizando SX2')
    ExecSX2(cEmpresa)

    oProcess:IncRegua2( 'Atualizando SX4')
    ExecSX4()

    oProcess:IncRegua2( 'Atualizando SX5')
    ExecSX5()

    oProcess:IncRegua2( 'Atualizando SX6')
    ExecSX6()

    oProcess:IncRegua2( 'Atualizando SX7')
    ExecSX7()

    oProcess:IncRegua2( 'Atualizando SIX')
    ExecSIX()

    oProcess:IncRegua2( 'Atualizando SXA')
    ExecSXA()

    oProcess:IncRegua2( 'Atualizando SXB')
    ExecSXB()

Return

/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX1()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX2(cEmpresa)
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX3()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX4()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX5()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX6()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSX7()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSIX()
    Sleep(100)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSXA()
    Sleep(200)
Return


/*/{Protheus.doc}

Atualiza dicionario SX1

@author  Fernando Alencar
@since   20/11/2011
/*/
Static Function ExecSXB()
    Sleep(200)
Return
