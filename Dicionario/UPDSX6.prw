#INCLUDE 'PROTHEUS.CH'

/*{ProtheusDoc} UPDSIX

Abstra��o do dicionario SX6

@author		fernando.alencar
@data		11 de novembro de 2011
@method		{create} 	cria um novo objeto
@method 	{add} 	 	adiciona um indice
@method 	{s}		 	seta os atributos do indice
@method 	{remove} 	remove um indice se existir
@method 	{clone} 	faz a copia das informacoes de um indice
@method		{confirm}	comita os indices adicionados
@method		{setlog}	seta o objeto responsavel pelo log
*/

CLASS UPDSX6

    DATA STRUCT
    DATA MODEL
    DATA MODELS //ACUMULA OS MODELOS AT� O CONFIRMA
    DATA OLOG
    DATA CFILIAL

    METHOD CREATE() CONSTRUCTOR
    METHOD ADD()
    METHOD S()
    METHOD REMOVE()
    METHOD CLONE()
    METHOD CONFIRM()
    METHOD SETLOG()

ENDCLASS

METHOD CREATE() CLASS UPDSX6

    ::STRUCT := { 'FIL' , 'VAR' , 'TIPO', 'DESCRIC', 'DSCSPA'  , ;
        'DSCENG', 'DESC1', 'DSCSPA1'   , 'DSCENG1','DESC2', 'DSCSPA2'   , 'DSCENG2',;
        'CONTEUD','CONTSPA' , 'CONTENG' , 'PROPRI', 'PYME', 'VALID', 'INIT','DEFPOR',;
        'DEFSPA', 'DEFENG' }

    ::MODELS := {}


    ::CFILIAL := SPACE(Len(cFilAnt))

    IF TYPE("OLOG") != "U"
        ::OLOG 	 := OLOG
    ELSE
        ::OLOG 	 := UPDLOG():CREATE()
    ENDIF

RETURN SELF

/*{ProtheusDoc} UPDSIX

Adiciona para inclus�o ou atualiza��o na base

@author		fernando.alencar
@data			20 de outubro de 2011
*/
METHOD ADD(CVAR, CTIPO, CDESCRICAO, CONTEUDO) CLASS UPDSX6

    ::MODEL := 	{ ::CFILIAL ,;//FIL
                    '' 		,;//VAR
                    ''		,;//TIPO
                    ''		,;//DESCRIC
                    ''		,;//DSCSPA
                    ''		,;//DSCENG
                    ''		,;//DESC1
                    ''		,;//DSCSPA1
                    ''  	,;//DSCENG1
                    ''  	,;//DESC2
                    ''  	,;//DSCSPA2
                    ''  	,;//DSCENG2
                    ''  	,;//CONTEUD
                    ''  	,;//CONTSPA
                    ''  	,;//CONTENG
                    'S'  	,;//PROPRI
                    'S'  	,;//PYME
                    ''  	,;//VALID
                    ''  	,;//INIT
                    ''  	,;//DEFPOR
                    ''  	,;//DEFSPA
                    ''		};//DEFENG

    AADD(::MODELS, ::MODEL)						  			  	//REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('VAR'	 , CVAR)
    SELF:S('TIPO'	 , CTIPO)
    SELF:S('CONTEUD' , CONTEUDO)
    SELF:S('DEFPOR'	 , CONTEUDO)
    SELF:S('DESCRIC' , CDESCRICAO)
    SELF:S('DESC1'	 , SUBSTR(CDESCRICAO,051))
    SELF:S('DESC2'	 , SUBSTR(CDESCRICAO,101))

RETURN SELF

/*
SETA O VALOR DO ATRIBUTO
*/
METHOD S(ATTR, VALOR) CLASS UPDSX6

    LOCAL nI := ASCAN(::STRUCT, ATTR)

    IF nI = 0
        ::OLOG:LOG("ATENCAO: N�o foi poss�vel configurar o atributo: "+ATTR+"!")
    ELSE
        ::MODEL[nI] := VALOR
    ENDIF

RETURN SELF

/*
REMOVE REGISTRO
*/
METHOD REMOVE(CFILIAL, CVARSX6) CLASS UPDSX6

    LOCAL AAREA := GETAREA()

    DBSELECTAREA("SX6")
    SX6->(DBSETORDER(1))
    SX6->(DBGOTOP())

    IF SX6->(DBSEEK(CFILIAL + CVARSX6))
        IF SX6->(RLock())
            SX6->(dbDelete())
            SX6->(dbUnLock())
        ENDIF
    ENDIF

    RESTAREA(AAREA)

RETURN .F.


/*/{Protheus.doc} CONFIRM
@description Faz copia de um indice.

@author  	Helitom Silva
@since   	16/04/2014
@version 	1.0

/*/
METHOD CONFIRM() CLASS UPDSX6

    LOCAL nI

    ::OLOG:LOG("Os seguintes param�tros foram adicionados ao dicion�rio SX6")

    DBSELECTAREA("SX6")
    SX6->(DBSETORDER(1))

    FOR nI := 1 TO LEN(::MODELS)

        ::OLOG:LOG(" PARAM�TRO ["+::MODELS[nI][02]+"]")

        IIF(SX6->(DBSEEK(::MODELS[nI][01]+::MODELS[nI][02])), SX6->(RLock()), SX6->(dbAppend()))

        SX6->X6_FIL    		:= ::MODELS[nI][01]
        SX6->X6_VAR     	:= ::MODELS[nI][02]
        SX6->X6_TIPO     	:= ::MODELS[nI][03]
        SX6->X6_DESCRIC 	:= ::MODELS[nI][04]
        SX6->X6_DSCSPA		:= ::MODELS[nI][05]
        SX6->X6_DSCENG		:= ::MODELS[nI][06]
        SX6->X6_DESC1    	:= ::MODELS[nI][07]
        SX6->X6_DSCSPA1		:= ::MODELS[nI][08]
        SX6->X6_DSCENG1  	:= ::MODELS[nI][09]
        SX6->X6_DESC2  		:= ::MODELS[nI][10]
        SX6->X6_DSCSPA2  	:= ::MODELS[nI][11]
        SX6->X6_DSCENG2  	:= ::MODELS[nI][12]
        SX6->X6_CONTEUD		:= ::MODELS[nI][13]
        SX6->X6_CONTSPA		:= ::MODELS[nI][14]
        SX6->X6_CONTENG		:= ::MODELS[nI][15]
        SX6->X6_PROPRI 		:= ::MODELS[nI][16]
        SX6->X6_PYME  		:= ::MODELS[nI][17]
        SX6->X6_VALID  		:= ::MODELS[nI][18]
        SX6->X6_INIT  		:= ::MODELS[nI][19]
        SX6->X6_DEFPOR 		:= ::MODELS[nI][20]
        SX6->X6_DEFSPA 		:= ::MODELS[nI][21]
        SX6->X6_DEFENG 		:= ::MODELS[nI][22]

        DBUNLOCK()

    NEXT

    ::OLOG:LINE()

    ::MODELS := {}

RETURN SELF


/*/{Protheus.doc} CLONE
@description Faz copia de um indice.

@author  	Helitom Silva
@since   	16/04/2014
@version 	1.0

@author  	Geanderson Silva
@since   	30/01/2015
@version 	1.0
@obs		Mudan�a para modificar o parametro quando for exclusivo. 		

@param CPARAMORI, Caracter, Nome do Parametro a ser copiado (Parametro de Origem)
@param CPARAMDES, Caracter, Nome do Parametro a ser criado (Parametro de Destino)
@param CPARAMFIL, Caracter, Codigo da Filial do Parametro.

/*/
METHOD CLONE(CPARAMORI, CPARAMDES, CPARAMFIL) CLASS UPDSX6

    DEFAULT CPARAMORI := ''
    DEFAULT CPARAMDES := CPARAMORI
    DEFAULT CPARAMFIL := ::CFILIAL

    DBSELECTAREA('SX6')
    SX6->(DBSETORDER(1))
    SX6->(DBGOTOP())

    IF SX6->(DBSEEK(CPARAMFIL + CPARAMORI))

        ::MODEL := 	{ SX6->X6_FIL     ,;//FIL
        CPARAMDES       ,;//VAR
        SX6->X6_TIPO    ,;//TIPO
        SX6->X6_DESCRIC ,;//DESCRIC
        SX6->X6_DSCSPA  ,;//DSCSPA
        SX6->X6_DSCENG  ,;//DSCENG
        SX6->X6_DESC1   ,;//DESC1
        SX6->X6_DSCSPA1 ,;//DSCSPA1
        SX6->X6_DSCENG1 ,;//DSCENG1
        SX6->X6_DESC2   ,;//DESC2
        SX6->X6_DSCSPA2 ,;//DSCSPA2
        SX6->X6_DSCENG2 ,;//DSCENG2
        SX6->X6_CONTEUD ,;//CONTEUD
        SX6->X6_CONTSPA ,;//CONTSPA
        SX6->X6_CONTENG ,;//CONTENG
        SX6->X6_PROPRI  ,;//PROPRI
        SX6->X6_PYME    ,;//PYME
        SX6->X6_VALID   ,;//VALID
        SX6->X6_INIT    ,;//INIT
        SX6->X6_DEFPOR  ,;//DEFPOR
        SX6->X6_DEFSPA  ,;//DEFSPA
        SX6->X6_DEFENG  };//DEFENG

        AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    ELSE
        CONOUT("ATENCAO: Não pois possível criar o Parametro: " + CPARAMDES + ", pois o Parametro: " + CPARAMORI + "não existe no dicionário SX6!")
        ::OLOG:LOG("ATENCAO: Não pois possível criar o Parametro: " + CPARAMDES + ", pois o Parametro: " + CPARAMORI + "não existe no dicionário SX6!")
    ENDIF

RETURN SELF
