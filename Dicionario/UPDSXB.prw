#INCLUDE 'PROTHEUS.CH'


/*/{Protheus.doc} UPDSXB
@description Abstra��o do dicionario SXB

@author	Helitom Silva
@since	27/04/2012

@obs @method {create} 		cria um novo objeto
	 @method {addconsult()} adiciona consulta
	 @method {addindice()}	adiciona indice
	 @method {addcoluna()}	adiciona coluna
	 @method {addfiltro()}	adiciona filtro
	 @method {addretorn()}	adiciona retorno
	 @method {addbotinc()}	adiciona botao de inclusao
	 @method {addbotalt()}	adiciona botao de altera��o
	 @method {addbotdel()}	adiciona botao de dele��o
	 @method {s}		 	seta os atributos 
	 @method {remove} 		remove consulta
	 @method {confirm}		comita consulta
	 @method {setlog}		seta o objeto responsavel pelo log

/*/
CLASS UPDSXB

    DATA STRUCT
    DATA MODEL
    DATA MODELS //ACUMULA OS MODELOS AT� O CONFIRMA
    DATA OLOG

    METHOD CREATE() CONSTRUCTOR
    METHOD ADDCONSULT()
    METHOD ADDINDICE()
    METHOD ADDCOLUNA()
    METHOD ADDFILTRO()
    METHOD SETALLFILIAIS()
    METHOD ADDRETORN()
    METHOD ADDBOTINC()
    METHOD ADDBOTALT()
    METHOD ADDBOTDEL()
    METHOD S()
    METHOD REMOVE()
    METHOD CONFIRM()
    METHOD SETLOG()

ENDCLASS

/*  Breve descricao de como se comporta cada etapa para
    criacao de uma consulta Padrao.

    Exemplo: Consulta Posto de Coleta

	*** XB_TIPO := "1" - Primeira Etapa
	    Tipo que define informacoes de nome, descricoes e Tabela da consulta

		XB_ALIAS  := cAlias                      //Nome da consulta
		XB_TIPO   := "1"                         //Tipo que define informacoes de nome, descricoes e Tabela da consulta
		XB_SEQ    := "01"                        //Sequencia da consulta, neste caso 01.
		XB_COLUNA := "DB"                        //DB, define arquivo de banco
		XB_DESCRI := "Posto de Coleta"           //Descricao da consulta
		XB_CONTEM := "Z07"                       //Tabela a ser consultada

   *** XB_TIPO   := "2" - Segunda etapa
       Tipo que define os indices utilizados para consulta

		XB_ALIAS  := cAlias                      //Nome da consulta
		XB_TIPO   := "2"                         //Tipo que define os indices utilizados para consulta
		XB_SEQ    := "01"                        //Sequencia ordem que ira aparecer o indice
		XB_COLUNA := "01"                        //Numero de Ordem do Indice na Tabela de Consulta(Dicionario)
		XB_DESCRI := "Codigo do Posto"           //Descricao do Indice
		XB_CONTEM := ""
		
   *** XB_TIPO   := "3" - Terceira etapa
       Tipo que define os Botoes a serem apresentados na Consulta.

		XB_ALIAS  := cAlias                      //Nome da consulta
		XB_TIPO   := "3"                         Tipo que define os Botoes a serem apresentados na Consulta.
		XB_SEQ    := "01"                        //Sequencia ordem que ira aparecer o bot�o
		XB_COLUNA := "01"                        //Numero de Ordem do Bot�o
		XB_DESCRI := "Cadastra Novo"             //Descricao do Botao em Portugues.
		XB_DESSPA := "Registra Nuevo"            //Descricao do Botao em Espanhol.
		XB_DESENG := "Add New"           		 //Descricao do Botao em Ingles.
		XB_CONTEM := ""							 //Fun��o a Ser executada ao clicar no Bot�o

   *** XB_TIPO   := "4" - Quarta etapa
       Tipo que define os campos que aparecerao conforme os indices

		XB_ALIAS  := cAlias                      //Nome da consulta
		XB_TIPO   := "4"                         //Tipo que define os campos que aparecerao conforme os indices
		XB_SEQ    := "01"                        //Informe aqui o codigo da sequencia informada para a sequencia do indice do Tipo 2 (APPENDA ANTERIOR)
		XB_COLUNA := "01"                        //Sequencia ordem que ira aparecer o campo na consulta da esquerda para direita
		XB_DESCRI := "Codigo Posto"              //Descri�ao da coluna
		XB_CONTEM := "Z07_CODIGO"                //Field da tabela correspondente a coluna da consulta

   *** XB_TIPO   := "4" - Quarta etapa
       Tipo que define os campos que aparecerao conforme os indices

		XB_ALIAS  := cAlias                      //Nome da consulta
		XB_TIPO   := "4"                         //Tipo que define os campos que aparecerao conforme os indices
		XB_SEQ    := "01"                        //Informe aqui o codigo da sequencia informada para a sequencia do indice do Tipo 2
		XB_COLUNA := "02"                        //Sequencia ordem que ira aparecer o campo na consulta da esquerda para direita
		XB_DESCRI := "Descricao"                 //Descri�ao da coluna
		XB_CONTEM := "Z07_NOME"                  //Field da tabela correspondente a coluna da consulta

   *** XB_TIPO   := "5" - Quinta etapa
       Tipo que define o retorno da consulta

		XB_ALIAS  := cAlias                      //Nome da consulta
		XB_TIPO   := "5"                         //Tipo que define o retorno da consulta
		XB_SEQ    := "02"                        //Sequencia do Retorno
		XB_COLUNA := ""
		XB_DESCRI := ""
		XB_CONTEM := "Z07->Z07_CODIGO"           //Campo a ser retornado na consulta
		
   *** XB_TIPO   := "6" - Sexta etapa
       Tipo que define o filtro da consulta

		XB_ALIAS  := cAlias                    //Nome da consulta
		XB_TIPO   := "6"                       //Tipo que define o filtro que ser� realizado na consulta
		XB_SEQ    := "01"                      //Sequencia
		XB_CONTEM := "tabela->campo = 1"       //Filtro da consulta
		
   *** XB_TIPO   := "7" - S�tima etapa (Exemplo SRA02A) - Usado para aparecer todas as filiais
       Tipo que define o filtro padr�o da Consula

		XB_ALIAS  := cAlias                    //Nome da consulta
		XB_TIPO   := "7"                       //Tipo que define o filtro que ser� realizado na consulta
		XB_SEQ    := "01"                      //Sequencia
		XB_CONTEM := "Space(FWSizeFilial())"   //Filtro da consulta de filiais

		XB_ALIAS  := cAlias                    //Nome da consulta
		XB_TIPO   := "7"                       //Tipo que define o filtro que ser� realizado na consulta
		XB_SEQ    := "02"                      //Sequencia
		XB_CONTEM := "Replicate('Z', FWSizeFilial())"       //Filtro da consulta de filiais
   		
*/

/*/{Protheus.doc} CREATE
@description Metodo Construtor da Classe UPDSXB

@author  Helitom Silva
@since   02/03/2016
@version 1.0

/*/
METHOD CREATE() CLASS UPDSXB

    ::STRUCT := { 'ALIAS' , 'TIPO' , 'SEQ', 'COLUNA', 'DESCRI'  , ;
        'DESCSPA'  ,'DESCENG'  ,'CONTEM'}
    ::MODELS := {}

    IF TYPE("OLOG") != "U"
        ::OLOG 	 := OLOG
    ELSE
        ::OLOG 	 := UPDLOG():CREATE()
    ENDIF

RETURN SELF


/*/{Protheus.doc} ADDCONSULT
@Description Adiciona Consulta

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD ADDCONSULT(CCONSULTA, CDESCRI, CDESCSPA, CDESCENG, CTABELA, CTIPO) CLASS UPDSXB

	/*  XB_TIPO := "1" - Primeira e segunda etapa
	    Tipo que define informacoes de nome, descricoes e Tabela da consulta

		XB_ALIAS   := cAlias                      //Nome da consulta
		XB_TIPO    := "1"                         //Tipo que define informacoes de nome, descricoes e Tabela da consulta
		XB_SEQ     := "01"                        //Sequencia da consulta, neste caso 01.
		XB_COLUNA  := "DB"                        //Define tipo de Consulta: Banco (BD), Especifica (RE)
		XB_DESCRI  := "Posto de Coleta"           //Descricao da consulta PORTUGUES
		XB_DESCSPA := "Posto de Coleta"           //Descricao da consulta ESPANHOL
		XB_DESCENG := "Posto de Coleta"           //Descricao da consulta INGLES
		XB_CONTEM  := "Z07"                       //Tabela a ser consultada
	*/

    DO CASE
    CASE CTIPO = '1' //Consulta de banco de dados
        CTIPO := 'DB'
    CASE CTIPO = '2' //Consulta Especifica
        CTIPO := 'RE'
    OTHERWISE
        CTIPO := 'DB'
    ENDCASE

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '1'	,;//XB_TIPO
    '01'	,;//XB_SEQ
    'DB'   ,;//XB_COLUNA
    ''		,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS'	, CCONSULTA)
    SELF:S('DESCRI' , CDESCRI)
    SELF:S('DESCSPA', CDESCSPA)
    SELF:S('DESCENG', CDESCENG)
    SELF:S('CONTEM' , CTABELA)
    SELF:S('COLUNA' , CTIPO)

RETURN SELF


/*/{Protheus.doc} ADDINDICE
@Description Adiciona Indice a Consulta

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD ADDINDICE(CCONSULTA, CSEQINDEX, CCODINDEX, CDESCRI, CDESCSPA, CDESCENG, CFORMULA) CLASS UPDSXB

   /*  XB_TIPO   := "2" - Terceira etapa
       Tipo que define os indices utilizados para consulta

		XB_ALIAS   := cAlias                     //Nome da consulta
		XB_TIPO    := "2"                        //Tipo que define os indices utilizados para consulta
		XB_SEQ     := "01"                       //Sequencia ordem que ira aparecer o indice
		XB_COLUNA  := "01"                       //Numero de Ordem do Indice na Tabela de Consulta(Dicionario)
		XB_DESCRI  := "Posto de Coleta"          //Descricao da consulta PORTUGUES
		XB_DESCSPA := "Posto de Coleta"          //Descricao da consulta ESPANHOL
		XB_DESCENG := "Posto de Coleta"          //Descricao da consulta INGLES
   */

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '2'	,;//XB_TIPO
    ''		,;//XB_SEQ
    ''     ,;//XB_COLUNA
    ''		,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS'	, CCONSULTA)
    SELF:S('SEQ'	, CSEQINDEX)
    SELF:S('COLUNA' , CCODINDEX)
    SELF:S('DESCRI' , CDESCRI)
    SELF:S('DESCSPA', CDESCSPA)
    SELF:S('DESCENG', CDESCENG)
    SELF:S('CONTEM' , CFORMULA)

RETURN SELF


/*/{Protheus.doc} ADDCOLUNA
@Description Adiciona Coluna a Consulta

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD ADDCOLUNA(CCONSULTA, CSEQINDEX, CSEQCOLUNA, CDESCRI, CDESCSPA, CDESCENG, CFIELDTAB) CLASS UPDSXB

   /*  XB_TIPO   := "4" - Quarta etapa
       Tipo que define os campos que aparecerao conforme os indices

		XB_ALIAS   := cAlias                      //Nome da consulta
		XB_TIPO    := "4"                         //Tipo que define os campos que aparecerao conforme os indices
		XB_SEQ     := "01"                        //Informe aqui o codigo da sequencia informada para a sequencia do indice do Tipo 2
		XB_COLUNA  := "02"                        //Sequencia ordem que ira aparecer o campo na consulta da esquerda para direita
		XB_DESCRI  := "Posto de Coleta"          //Descricao da consulta PORTUGUES
		XB_DESCSPA := "Posto de Coleta"          //Descricao da consulta ESPANHOL
		XB_DESCENG := "Posto de Coleta"          //Descricao da consulta INGLES
		XB_CONTEM  := "Z07_NOME"                  //Field da tabela correspondente a coluna da consulta
   */

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '4'	,;//XB_TIPO
    ''		,;//XB_SEQ
    'DB'	,;//XB_COLUNA
    ''	    ,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS'	, CCONSULTA)
    SELF:S('SEQ'	, CSEQINDEX)
    SELF:S('COLUNA'	, CSEQCOLUNA)
    SELF:S('DESCRI'	, CDESCRI)
    SELF:S('DESCSPA', CDESCSPA)
    SELF:S('DESCENG', CDESCENG)
    SELF:S('CONTEM'	, CFIELDTAB)

RETURN SELF


/*/{Protheus.doc} ADDFILTRO
@Description Adiciona Filtro a Consulta

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD ADDFILTRO(CCONSULTA, CFILTRO) CLASS UPDSXB

   /* XB_TIPO   := "6" - Sexta etapa
       Tipo que define o filtro da consulta

		XB_ALIAS  := cAlias                    //Nome da consulta
		XB_TIPO   := "6"                       //Tipo que define o filtro que ser� realizado na consulta
		XB_SEQ    := "01"                      //Sequencia
		XB_CONTEM := "tabela->campo = 1"       //Filtro da consulta
   */

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '6'	,;//XB_TIPO
    '01'	,;//XB_SEQ
    ''     ,;//XB_COLUNA
    ''		,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS'	, CCONSULTA)
    SELF:S('CONTEM'	, CFILTRO)

RETURN SELF


/*/{Protheus.doc} SETALLFILIAIS
@Description Mostra registros de todas as filiais (Exemplo:  SRA02A)

@author	Helitom Silva
@since	29/03/2019

/*/
METHOD SETALLFILIAIS(CCONSULTA) CLASS UPDSXB

   /* XB_TIPO   := "7" - Setima etapa
       Tipo que define o filtro da consulta

		XB_ALIAS  := cAlias                    //Nome da consulta
		XB_TIPO   := "7"                       //Tipo que define o filtro que ser� realizado na consulta
		XB_SEQ    := "01"                      //Sequencia
		XB_CONTEM := "Space(FWSizeFilial())"       //Filtro da consulta
   */

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '7'	,;//XB_TIPO
    '01'	,;//XB_SEQ
    ''     ,;//XB_COLUNA
    ''		,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS'	, CCONSULTA)
    SELF:S('CONTEM', "Space(FWSizeFilial())")

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '7'	,;//XB_TIPO
    '02'	,;//XB_SEQ
    ''     ,;//XB_COLUNA
    ''		,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS', CCONSULTA)
    SELF:S('CONTEM', "Replicate('Z', FWSizeFilial())")

RETURN SELF


/*/{Protheus.doc} ADDFILTRO
@Description Adiciona Retorno a Consulta

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD ADDRETORN(CCONSULTA, CSEQRET, CRETORNO) CLASS UPDSXB

   /* XB_TIPO   := "5" - Quinta etapa
      Tipo que define o retorno da consulta

	  XB_ALIAS  := cAlias                      //Nome da consulta
	  XB_TIPO   := "5"                         //Tipo que define o retorno da consulta
	  XB_SEQ    := "02"                        //Sequencia do Retorno
	  XB_CONTEM := "Z07->Z07_CODIGO"           //Campo a ser retornado na consulta
   */

    ::MODEL := 	{    '' 	,;//XB_ALIAS
    '5'	,;//XB_TIPO
    ''		,;//XB_SEQ
    ''	    ,;//XB_COLUNA
    ''		,;//XB_DESCRI
    ''		,;//XB_DESCSPA
    ''		,;//XB_DESCENG
    ''		} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS'	, CCONSULTA)
    SELF:S('SEQ'	, CSEQRET)
    SELF:S('CONTEM'	, CRETORNO)

RETURN SELF


/*/{Protheus.doc} ADDFILTRO
@Description Adiciona Botao de Inclus�o

@author	Helitom Silva
@since	28/03/2014

/*/
METHOD ADDBOTINC(CCONSULTA) CLASS UPDSXB

    ::MODEL := 	{ '' 				,;//XB_ALIAS
    '3'				,;//XB_TIPO
    '01'				,;//XB_SEQ
    '01'  			,;//XB_COLUNA
    'Cadastra Novo'	,;//XB_DESCRI
    'Cadastra Nuevo'	,;//XB_DESCSPA
    'Add New'			,;//XB_DESCENG
    '01'				} //XB_CONTEM

    AADD(::MODELS, ::MODEL)	 //REGISTRA O MODELO NA LISTA DE MODELOS

    SELF:S('ALIAS', CCONSULTA)

RETURN SELF


/*/{Protheus.doc} ADDBOTALT
@Description Adiciona Botao de Altera��o

@author	Helitom Silva
@since	28/03/2014

/*/
METHOD ADDBOTALT(CCONSULTA) CLASS UPDSXB

    Local nPosCons 	  := aScan(::MODELS, {|X| AllTrim(X[1]) == AllTrim(CCONSULTA) .and. AllTrim(X[2]) == '1'})
    Local nPosCont 	  := aScan(::STRUCT, {|X| AllTrim(X) == 'CONTEM'})

    Local cAliasTemp  := AllTrim(::MODELS[nPosCons][nPosCont])

    ::MODELS[nPosCons][nPosCont] := cAliasTemp + 'A'

RETURN SELF


/*/{Protheus.doc} ADDBOTDEL
@Description Adiciona Botao de Exclus�o

@author	Helitom Silva
@since	28/03/2014

/*/
METHOD ADDBOTDEL(CCONSULTA) CLASS UPDSXB

    Local nPosCons 	  := aScan(::MODELS, {|X| AllTrim(X[1]) == AllTrim(CCONSULTA) .and. AllTrim(X[2]) == '1'})
    Local nPosCont 	  := aScan(::STRUCT, {|X| AllTrim(X) == 'CONTEM'})

    Local cAliasTemp  := AllTrim(::MODELS[nPosCons][nPosCont])

    ::MODELS[nPosCons][nPosCont] := cAliasTemp + 'D'

RETURN SELF


/*/{Protheus.doc} S
@Description Seta valor ao Atributo

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD S(ATTR, VALOR) CLASS UPDSXB

    LOCAL nI := ASCAN(::STRUCT, ATTR)

    IF nI = 0
        ::OLOG:LOG("ATENCAO: N�o foi poss�vel configurar o atributo: "+ATTR+"!")
    ELSE
        ::MODEL[nI] := VALOR
    ENDIF

RETURN SELF


/*/{Protheus.doc} CONFIRM
@Description Confirma a Grava��o do Registro

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD CONFIRM() CLASS UPDSXB

    LOCAL nI

    ::OLOG:LOG("As seguintes Etapas da consulta foram adicionados ao dicion�rio SXB")

    DBSELECTAREA("SXB")
    SXB->(DBSETORDER(1))

    FOR nI := 1 TO LEN(::MODELS)

        ::OLOG:LOG(" A Etapa Tipo:"+::MODELS[nI][02]+", SEQ:"+::MODELS[nI][03]+"]")

        SXB->(dbGoTop())
        IIf(SXB->(dbSeek(PADR(::MODELS[nI][01],06)+::MODELS[nI][02]+::MODELS[nI][03]+::MODELS[nI][04])), SXB->(RLock()), SXB->(dbAppend()))
        SXB->XB_ALIAS   := ::MODELS[nI][01]
        SXB->XB_TIPO    := ::MODELS[nI][02]
        SXB->XB_SEQ     := ::MODELS[nI][03]
        SXB->XB_COLUNA  := ::MODELS[nI][04]
        SXB->XB_DESCRI  := ::MODELS[nI][05]
        SXB->XB_DESCSPA := ::MODELS[nI][06]
        SXB->XB_DESCENG := ::MODELS[nI][07]
        SXB->XB_CONTEM  := ::MODELS[nI][08]
        SXB->(DBUNLOCK())

    NEXT

    ::OLOG:LINE()

    ::MODELS := {}

RETURN SELF


/*/{Protheus.doc} REMOVE
@Description Remove consulta

@author	Helitom Silva
@since	27/04/2012

/*/
METHOD REMOVE(CCONSULTA) CLASS UPDSXB

    DBSELECTAREA("SXB")
    SXB->(DBSETORDER(1))

    SXB->(dbGoTop())
    If SXB->(dbSeek(CCONSULTA))
        While .not. SXB->(Eof()) .and. AllTrim(SXB->XB_ALIAS) == AllTrim(CCONSULTA)
            If SXB->(RLock())
                SXB->(dbDelete())
                SXB->(dbUnLock())
            EndIf
            SXB->(dbSkip())
        EndDo
    EndIf

RETURN SELF


/*/{Protheus.doc} SETLOG
@Description Seta Log

@author Helitom Silva
@since	27/04/2012

/*/
METHOD SETLOG(OLOG) CLASS UPDSXB

    ::OLOG := OLOG

RETURN SELF