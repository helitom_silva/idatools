#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'IDATOOLS.CH'


/*/{Protheus.doc} IdaParam
@description Classe para menipula��o e Transferencia de Parametros

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@obs A estrutura do Objeto correspode ao formato abaixo:
	 Parametros{{Param1:Nome, Param1:Value, Param1:Type}, {Param2:Nome, Param2:Value, Param2:Type}...}
	
/*/
Class IdaParam

    Data aParam Hidden

    Method New(p_aParam) Constructor
    Method Add(p_cParam, p_uValue, p_cType)
    Method GetValue(p_cParam)
    Method GetType(p_cParam)
    Method GetParams()
    Method GetJson()

EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da Classe

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@param p_aParam, Array, Array na estrutura Padr�o formato: {{'cPARAMETRO1', uVALOR, cTIPO}, {'cPARAMETRO2', uVALOR, cTIPO}...} 

/*/
Method New(p_aParam) Class IdaParam

    Local nItPar := 0

    Default p_aParam := {}

    Self:aParam := {}

    If Len(p_aParam) > 0

        For nItPar := 1 To Len(p_aParam)
            Aadd(Self:aParam, IdaParIt():New(p_aParam[nItPar][1], p_aParam[nItPar][2], p_aParam[nItPar][3]))
        Next

    EndIf

Return


/*/{Protheus.doc} Add
@description Adiciona Novo Parametro no objeto

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@param p_cParam, Carcatrer, descricao
@param p_uValue, Indefinido, descricao
@param p_cType, Typo, {N-Numerico, C-Caracter, D-Data, L-Logico, O-Objeto}

/*/
Method Add(p_cParam, p_uValue, p_cType) Class IdaParam

    Default p_cParam := ''
    Default p_uValue := Nil
    Default p_cType  := ValType(p_uValue)

    Aadd(Self:aParam, IdaParIt():New(p_cParam, p_uValue, p_cType))

Return


/*/{Protheus.doc} GetValue
@description Obtem valor do parametro

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@param p_cParam, Caracter, Nome do Parametro

@return uRet, Indefinido, Valor do Parametro

/*/
Method GetValue(p_cParam) Class IdaParam

    Local uRet   := Nil
    Local nItPar := 0

    Default p_cParam := ''

    If Len(Self:aParam) > 0

        If (nItPar := Ascan(Self:aParam, {|X| X:GetParam() = p_cParam})) > 0
            uRet := Self:aParam[nItPar]:GetValue()
        EndIf

    EndIf

Return uRet


/*/{Protheus.doc} GetType
@description Obtem o Typo do Parametro

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@param p_cParam, Caracter, Nome do parametro

/*/
Method GetType(p_cParam) Class IdaParam

    Local cRet   := ''
    Local nItPar := 0

    Default p_cParam := ''

    If Len(Self:aParam) > 0

        If (nItPar := Ascan(Self:aParam, {|X| X:GetParam() = p_cParam})) > 0
            cRet := Self:aParam[nItPar]:GetType()
        EndIf

    EndIf

Return cRet


/*/{Protheus.doc} GetParams
@description Obtem lista de Parametros

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@return aRet, Array, Lista de parametros no formato objeto da Classe IdaParIt - Parametros{{Param1:GetParam(), Param1:GetValue(), Param1:GetType()}, {Param2:GetParam(), Param2:GetValue(), Param2:GetType()}...}

/*/
Method GetParams() Class IdaParam

    Local aRet := Clone(Self:aParam)

Return aRet


/*/{Protheus.doc} GetParams
@description Obtem Lista de Parametros 

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@return cRet, Array, Lista de parametros no formato Json

/*/
Method GetJson() Class IdaParam

    Local cRet := FWJsonSerialize(Self, .F., .F.)

Return cRet


/*/{Protheus.doc} GetParams
@description Classe Parametro Item

@author  Helitom Silva
@since   29/05/2018
@version 1.0

/*/
    Class IdaParIt

        Data cParam Hidden
        Data uValue Hidden
        Data cType  Hidden

        Method New() Constructor
        Method GetParam()
        Method GetValue()
        Method GetType()
        Method GetJson()

    EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da IdaParIt

@author  Helitom Silva
@since   29/05/2018
@version 1.0

/*/
Method New(p_cParam, p_uValue, p_cType) Class IdaParIt

    Default p_cParam := ''
    Default p_uValue := Nil
    Default p_cType  := ValType(p_uValue)

    Self:cParam := p_cParam
    Self:uValue := p_uValue
    Self:cType  := p_cType

Return Self


/*/{Protheus.doc} GetParam
@description Obtem Nome do Parametro

@author  Helitom Silva
@since   29/05/2018
@version 1.0

/*/
Method GetParam() Class IdaParIt
Return Self:cParam


/*/{Protheus.doc} GetValue
@description Obtem Valor do Parametro

@author  Helitom Silva
@since   29/05/2018
@version 1.0

/*/
Method GetValue() Class IdaParIt
Return Self:uValue


/*/{Protheus.doc} GetType
@description Obtem Tipo do Parametro

@author  Helitom Silva
@since   29/05/2018
@version 1.0

/*/
Method GetType() Class IdaParIt
Return Self:cType


/*/{Protheus.doc} GetJson
@description Obtem Lista de Parametros 

@author  Helitom Silva
@since   29/05/2018
@version 1.0

@return cRet, Caracter, Objeto no formato JSON

/*/
Method GetJson() Class IdaParIt

    Local cRet := FWJsonSerialize(Self, .F., .F.)

Return cRet