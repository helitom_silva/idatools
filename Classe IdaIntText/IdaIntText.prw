#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'IDATOOLS.CH'


/*/{Protheus.doc} IdaIntText
@description Classe para manipula��o de arquivo de integra��o via Texto

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@obs A estrutura do Objeto corresponde ao formato abaixo:
	 Parametros{{Field1:Nome, Field1:Value, Field1:Type}, {Param2:Nome, Param2:Value, Param2:Type}...}
	
/*/
Class IdaIntText

    Data aFields
    Data aLines
    Data nLine

    Method New(p_aFields) Constructor
    Method AddField(p_cSeq, p_nInit, p_nByte, p_cType, p_cInOut, p_cField, p_cTitle, p_cDescription, p_cNote, p_nDecimal)
    Method AddLine()
    Method SetLine()
    Method GetLine()
    Method GetQttLine()
    Method GetQttField()
    Method SetText(p_cText)
    Method GetText(p_cText)
    Method SetValue(p_cField, p_cValue)
    Method GetValue(p_cField)
    Method GetSeq(p_cField)
    Method GetInit(p_cField)
    Method GetByte(p_cField)
    Method GetType(p_cField)
    Method GetInOut(p_cField)
    Method GetTitle(p_cField)
    Method GetDescription(p_cField)
    Method GetNote(p_cField)
    Method GetDecimal(p_cField)
    Method GetListFields()
    Method GetJson()

EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da Classe

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_aFields, Array, Array na estrutura Padr�o formato: {{cSeq, nInit, nByte, cType, cInOut, cField, cTitle, cDescription, cNote, nDecimal}, {cSeq, nInit, nByte, cType, cInOut, cField, cTitle, cDescription, cNote, nDecimal}...} 

/*/
Method New(p_aFields) Class IdaIntText

    Local nItFld := 0

    Default p_aFields := {}

    Self:aFields := {}
    Self:aLines  := {}
    Self:nLine   := 0

    If Len(p_aFields) > 0

        For nItFld := 1 To Len(p_aFields)
            Self:Add(p_aFields[nItFld][1], p_aFields[nItFld][2], p_aFields[nItFld][3], p_aFields[nItFld][4], p_aFields[nItFld][5], p_aFields[nItFld][6], p_aFields[nItFld][7], p_aFields[nItFld][8], p_aFields[nItFld][9], p_aFields[nItFld][10])
        Next

        Self:AddLine()

    EndIf

Return


/*/{Protheus.doc} Add
@description Adiciona Novo Parametro no objeto

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Carcatrer, descricao
@param p_cValue, Indefinido, descricao
@param p_cType, Typo, {N-Numerico, C-Caracter, D-Data, L-Logico, O-Objeto}
@Param p_nInit, Caracter, Posicao inicial do campo dentro do Texto
@Param p_nByte, Caracter, Posicao final do campo dentro do Texto
@Param p_nDecimal, Caracter, quantidade de digitos que representam os decimais, para quando o campo for do tipo N=Num�rico
@Param p_cDescription, Caracter, Descri��o da informa��o que � carregada dentro do texto

/*/
Method AddField(p_cSeq, p_nInit, p_nByte, p_cType, p_cInOut, p_cField, p_cTitle, p_cDescription, p_cNote, p_nDecimal) Class IdaIntText

    Default p_cSeq          := ''
    Default p_nInit         := 0
    Default p_nByte         := 0
    Default p_cType         := 'C'
    Default p_cInOut        := ''
    Default p_cField        := ''
    Default p_cTitle        := ''
    Default p_cDescription  := ''
    Default p_cNote         := ''
    Default p_nDecimal      := 0

    Aadd(Self:aFields, IdaIntFld():New(p_cSeq, p_nInit, p_nByte, p_cType, p_cInOut, p_cField, p_cTitle, p_cDescription, p_cNote, p_nDecimal))

Return


/*/{Protheus.doc} AddLine
@description Adiciona Novo Parametro no objeto

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Carcatrer, descricao
@param p_cValue, Indefinido, descricao
@param p_cType, Typo, {N-Numerico, C-Caracter, D-Data, L-Logico, O-Objeto}
@Param p_nInit, Caracter, Posicao inicial do campo dentro do Texto
@Param p_nByte, Caracter, Posicao final do campo dentro do Texto
@Param p_nDecimal, Caracter, quantidade de digitos que representam os decimais, para quando o campo for do tipo N=Num�rico
@Param p_cDescription, Caracter, Descri��o da informa��o que � carregada dentro do texto

/*/
Method AddLine() Class IdaIntText

    Aadd(Self:aLines, {})

    Self:SetLine(Len(Self:aLines))

    For nX := 1 to Len(Self:aFields)
        Aadd(Self:aLines[Self:nLine], RetDado(Self:aFields[nX]:GetType(), Self:aFields[nX]:GetByte()))
    Next

Return


/*/{Protheus.doc} SetLine
@description Define o Numero de Linha atual

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method SetLine(p_nLine) Class IdaIntText

    Default p_nLine := 0

    Self:nLine := p_nLine

Return Self:nLine


/*/{Protheus.doc} GetLine
@description Obtem a quantidade de Linhas

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetLine() Class IdaIntText
Return Self:nLine


/*/{Protheus.doc} GetLine
@description Obtem a quantidade de Linhas

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetQttLine() Class IdaIntText
Return Len(Self:aLines)


Method GetQttField() Class IdaIntText
Return Len(Self:aFields)


/*/{Protheus.doc} SetText
@description Define o texto que ser� particionado conforme posi��o dos campos da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cText, Caracter, Texto a ser particionado

@return lRet, Logico, Se conseguiu particionar e registrar os valores retorna .T.

/*/
Method SetText(p_cText, p_lNewLine) Class IdaIntText

    Local lRet   := .T.
    Local nItFld := 0
    Local cValue := ''

    Default p_cText := ''
    Default p_lNewLine := .T.

    If Len(Self:aFields) > 0

        If p_lNewLine
            Self:AddLine()
        EndIf

        For nItFld := 1 to Len(Self:aFields)
            
            cValue := Substr(p_cText, Self:aFields[nItFld]:GetInit(), Self:aFields[nItFld]:GetByte())

            Self:SetValue(Self:aFields[nItFld]:GetField(), cValue)

        Next

    Else
        lRet := .F.
    EndIf

Return lRet


/*/{Protheus.doc} GetText
@description Obtem o texto aglutinado conforme posi��o dos campos da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cText, Caracter, Texto a ser particionado

@return cRet, Caracter, Texto completo

/*/
Method GetText(p_nLine) Class IdaIntText

    Local cRet     := ''
    Local nItFld   := 0

    Default p_nLine := 0

    If p_nLine > 0
        Self:SetLine(p_nLine)
    EndIf

    For nItFld := 1 to Len(Self:aFields)

        cRet += PadR(Self:GetValue(Self:aFields[nItFld]:GetField()), Self:aFields[nItFld]:GetByte())

    Next
            
Return cRet


/*/{Protheus.doc} SetValue
@description Define valor da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Caracter, Nome da Informa��o
@param p_cField, Caracter, Nome da Informa��o

@return lRet, Logico, Se definiu valor retorna .T.

/*/
Method SetValue(p_cField, p_cValue) Class IdaIntText

    Local lRet   := .T.
    Local nItFld := 0

    Default p_cField := ''
    Default p_cValue := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0

            If Empty(p_cValue)
                p_cValue := PadR(p_cValue, Self:aFields[nItFld]:GetByte())
            EndIf

            Self:aLines[Self:nLine][nItFld] := p_cValue
        Else
            lRet := .F.
        EndIf

    Else
        lRet := .F.
    EndIf

Return lRet


/*/{Protheus.doc} GetValue
@description Obtem valor da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Caracter, Nome da Informa��o

@return uRet, Indefinido, Valor da Informa��o

/*/
Method GetValue(p_cField) Class IdaIntText

    Local uRet   := Nil
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            uRet := Self:aLines[Self:GetLine()][nItFld]
        EndIf

    EndIf

Return uRet


Method GetSeq(p_cField) Class IdaIntText

    Local cRet   := ''
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            cRet := Self:aFields[nItFld]:GetSeq()
        EndIf

    EndIf

Return cRet


/*/{Protheus.doc} GetInit
@description Obtem posi��o inicial da Informa��o dentro do texto

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Caracter, Nome do campo

/*/
Method GetInit(p_cField) Class IdaIntText

    Local nRet   := 0
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            nRet := Self:aFields[nItFld]:GetInit()
        EndIf

    EndIf

Return nRet


/*/{Protheus.doc} GetByte
@description Obtem posi��o final da Informa��o dentro do texto

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Caracter, Nome do campo

/*/
Method GetByte(p_cField) Class IdaIntText

    Local nRet   := 0
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            nRet := Self:aFields[nItFld]:GetByte()
        EndIf

    EndIf

Return nRet


/*/{Protheus.doc} GetType
@description Obtem o Tipo da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Caracter, Nome da Informa��o

/*/
Method GetType(p_cField) Class IdaIntText

    Local cRet   := ''
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            cRet := Self:aFields[nItFld]:GetType()
        EndIf

    EndIf

Return cRet


Method GetInOut(p_cField) Class IdaIntText

    Local cRet   := ''
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            cRet := Self:aFields[nItFld]:GetInOut()
        EndIf

    EndIf

Return cRet


Method GetTitle(p_cField) Class IdaIntText

    Local cRet   := ''
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            cRet := Self:aFields[nItFld]:GetTitle()
        EndIf

    EndIf

Return cRet


Method GetDescription(p_cField) Class IdaIntText

    Local cRet   := ''
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            cRet := Self:aFields[nItFld]:GetDescription()
        EndIf

    EndIf

Return cRet


Method GetNote(p_cField) Class IdaIntText

    Local cRet   := ''
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0
            cRet := Self:aFields[nItFld]:GetNote()
        EndIf

    EndIf

Return cRet


/*/{Protheus.doc} GetDecimal
@description Obtem numero de digitos decimais da Informa��o quando for do tipo N=Numerico

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@param p_cField, Caracter, Nome do campo

/*/
Method GetDecimal(p_cField, p_cPropertie) Class IdaIntText

    Local nRet   := 0
    Local nItFld := 0

    Default p_cField := ''

    If Len(Self:aFields) > 0

        If (nItFld := Ascan(Self:aFields, {|X| X:GetField() = p_cField})) > 0

            If p_cPropertie = 'SEQ'
                nRet := Self:aFields[nItFld]:GetDecimal()
            EndIf

        EndIf

    EndIf

Return nRet


/*/{Protheus.doc} GetListFields
@description Obtem lista de Campos

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@return aRet, Array, Lista de Campos no formato objeto da Classe IdaIntFld - Parametros{{Field1:GetListFields(), Field1:GetValue(), Field1:GetType(), Field1:GetInit(), Field1:GetByte(), Field1:GetTDecimal(), Field1:GetDescription()}, {Param2:...}...}

/*/
Method GetListFields() Class IdaIntText

    Local aRet := Self:aFields

Return aRet


/*/{Protheus.doc} GetListFields
@description Obtem Lista de Parametros 

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@return cRet, Array, Lista de parametros no formato Json

/*/
Method GetJson() Class IdaIntText

    Local cRet := FWJsonSerialize(Self, .F., .F.)

Return cRet


/*/{Protheus.doc} IdaIntFld
@description Classe Campo Item

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Class IdaIntFld

    Data cSeq
    Data nInit
    Data nByte
    Data cType
    Data cInOut
    Data cField    
    Data cTitle
    Data cDescription
    Data cNote
    Data nDecimal

    Method New() Constructor
    Method GetSeq()
    Method GetInit()
    Method GetByte()
    Method GetType()
    Method GetInOut()
    Method GetField()
    Method GetTitle()
    Method GetDescription()
    Method GetNote()
    Method GetDecimal()
    Method GetJson()

EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da IdaIntFld

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@Param p_cField, Caracter, Nome do campo
@Param p_cType, Caracter, Tipo da informa��o {N-Numerico, C-Caracter, D-Data, L-Logico, O-Objeto}
@Param p_nInit, Caracter, Posicao inicial do campo dentro do Texto
@Param p_nByte, Caracter, Posicao final do campo dentro do Texto
@Param p_nDecimal, Caracter, quantidade de digitos que representam os decimais, para quando o campo for do tipo N=Num�rico
@Param p_cDescription, Caracter, Descri��o da informa��o que � carregada dentro do texto

/*/
Method New(p_cSeq, p_nInit, p_nByte, p_cType, p_cInOut, p_cField, p_cTitle, p_cDescription, p_cNote, p_nDecimal) Class IdaIntFld

    Default p_cSeq          := ''
    Default p_nInit         := 0
    Default p_nByte         := 0
    Default p_cType         := 'C'
    Default p_cInOut        := ''
    Default p_cField        := ''
    Default p_cTitle        := ''
    Default p_cDescription  := ''
    Default p_cNote         := ''
    Default p_nDecimal      := 0

    Self:cSeq          := p_cSeq
    Self:nInit         := p_nInit
    Self:nByte         := p_nByte
    Self:cType         := p_cType
    Self:cInOut        := p_cInOut
    Self:cField        := p_cField
    Self:cTitle        := p_cTitle
    Self:cDescription  := p_cDescription
    Self:cNote         := p_cNote
    Self:nDecimal      := p_nDecimal

Return Self


Method GetSeq() Class IdaIntFld
Return Self:cSeq


/*/{Protheus.doc} GetInit
@description Obtem Posi��o Inicial da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetInit() Class IdaIntFld
Return Self:nInit


/*/{Protheus.doc} GetByte
@description Obtem Posi�ao Final da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetByte() Class IdaIntFld
Return Self:nByte


/*/{Protheus.doc} GetType
@description Obtem Tipo da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetType() Class IdaIntFld
Return Self:cType


Method GetInOut() Class IdaIntFld
Return Self:cInOut


/*/{Protheus.doc} GetField
@description Obtem Nome da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetField() Class IdaIntFld
Return Self:cField


Method GetTitle() Class IdaIntFld
Return Self:cTitle


/*/{Protheus.doc} GetDescription
@description Obtem Descri��o da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetDescription() Class IdaIntFld
Return Self:cDescription


Method GetNote() Class IdaIntFld
Return Self:cNote


/*/{Protheus.doc} GetDecimal
@description Obtem Numero de Digitos decimais da Informa��o

@author  Helitom Silva
@since   27/08/2018
@version 1.0

/*/
Method GetDecimal() Class IdaIntFld
Return Self:cDecimal

/*/{Protheus.doc} GetJson
@description Obtem Lista de Parametros 

@author  Helitom Silva
@since   27/08/2018
@version 1.0

@return cRet, Caracter, Objeto no formato JSON

/*/
Method GetJson() Class IdaIntFld

    Local cRet := FWJsonSerialize(Self, .F., .F.)

Return cRet
