#INCLUDE "RWMAKE.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"


/*/{Protheus.doc} IDACGMC
@description Consulta para sele��o de multiplos Itens.
  
@author  Helitom Silva
@since   02/02/2015
@version 1.0
  
@param p_aQuery, array, Query que seram utilizadas nas pesquisa.
@param p_lSelect, logico, Verifica se seleciona ou n�o a linha.
@param p_cTitulo, character, Titulo da tela de pesquisa.
@param p_aTit, array, Array contendo os titulos das colunas da grid.
@param p_aCols, array, Campos que seram preenchidos com o Select.
  
@return lRet, Retorna se conseguiu retornar o valor.
  
/*/
User Function IDACGMC(p_aQuery, p_lSelect, p_cTitulo, p_aTit, p_aCols)

    Local lRet		  := .T.

    Private oOk 	   := LoadBitmap(GetResources(), "LBOK")
    Private oNo 	   := LoadBitmap(GetResources(), "LBNO")
    Private aCbPesquis := {}
    Private cCbPesquis := ""
    Private cTexto	   := Space(25)
    Private lmarcaAll  := .F.
    Private lMultiSel  := .T.

    Public cIDACGMC	 := &(ReadVar())
    Public aIDACGMC	 := {}

    Default p_lSelect := .T.
    Default p_aTit    := {"C�digo", "Descri��o"}
    Default p_aCols	  := {"LLCODI", "LLDESC"}

    lMultiSel		  := p_lSelect
    aCbPesquis		  := p_aTit

    SetPrvt("oFntArial14", "oDlgPesq", "oSayPesq", "oCboPesq", "oGetText", "oBtnPesq", "oGridSel", "oCBoxMarc","oBtnOk","oBtnFech")

    oFntArial14 := TFont():New("Arial", 0, -12,, .F., 0,, 400, .F.,.F.,,,,,,)

    oDlgPesq := HSDialog():New(C(178), C(181), C(494), C(710), p_cTitulo,,, .F., DS_MODALFRAME,, /*Cor*/,,,.T.,,,.T.)

    oCboPesq := TComboBox():New(C(006), C(003), {|u| If(PCount() > 0, cCbPesquis := u, cCbPesquis)}, aCbPesquis, C(079), C(011), oDlgPesq,,,, CLR_BLACK, CLR_WHITE,.T., oFntArial14,"",,,,,,,"cCbPesquis" )
    oGetText := TGet():New(C(006), C(083), {|u| If(PCount() > 0, cTexto := u, cTexto)}, oDlgPesq, C(134), C(008),,,CLR_BLACK, CLR_WHITE, oFntArial14,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cTexto",,)
    oBtnPesq := TButton():New(C(006), C(219), "&Pesquisar", oDlgPesq, {|| PesqMarca(oCboPesq:nAt, cTexto)}, C(046), C(009),, oFntArial14,,.T.,,"",,,,.F. )

    oGridSel := TWBrowse():New(C(016), C(003),C(262), C(126),,{'', p_aTit[1], p_aTit[2]},, oDlgPesq,,,,,,,,,,,,.F.,,.T.,,.F.,,,)
    oGridSel:bLDblClick := {|| MarcaLinha(oGridSel:nAt)}
    oGridSel:SetArray(ListQuery(p_aQuery, p_aCols))
    oGridSel:bLine := {|| {oGridSel:aArray[oGridSel:nAt, 1], oGridSel:aArray[oGridSel:nAt, 02], oGridSel:aArray[oGridSel:nAt, 03]} }

    oCBoxMarc := TCheckBox():New( C(145), C(003), "Marcar/Desmarcar Todos", {|u| If(PCount() > 0, lmarcaAll := u, lmarcaAll) }, oDlgPesq, C(078), C(008),,, oFntArial14,, CLR_BLACK, CLR_WHITE,,.T., "lmarcaAll",,)
    oCBoxMarc:bChange := {|| MarcaLinha(,,lmarcaAll)}

    oBtnOk   := TButton():New(C(145), C(188), "&Ok", oDlgPesq, {|| lRet := Confirma(), oDlgPesq:End()}, C(037), C(009),, oFntArial14,,.T.,,"",,,,.F.)
    oBtnFech := TButton():New(C(145), C(228), "&Fechar", oDlgPesq, {|| lRet := .F., oDlgPesq:End()}, C(037), C(009),, oFntArial14,,.T.,,"",,,,.F.)

    oDlgPesq:Activate(,,,.T.,,,)

Return lRet


/*/{Protheus.doc} ListPar
@description Monta array com lista de valores da string informada no parametro

@author Helitom Silva
@since 12/02/2015

@param p_cCampo, Caracter, String com valor separado por |

@param aLista, Array, Array com valores da string

/*/
Static Function ListPar(p_cCampo)

    Local aLista := {}
    Local cTexto   := ''
    Local nX 		:= 0

    For nX := 1 to Len(p_cCampo)

        If SubStr(p_cCampo, nX, 1) = '|'
            aAdd(aLista, AllTrim(cTexto))
            cTexto := ''
        ElseIf nX = Len(p_cCampo)
            cTexto += SubStr(p_cCampo, nX, 1)
            aAdd(aLista, AllTrim(cTexto))
            cTexto := ''
        Else
            cTexto += SubStr(p_cCampo, nX, 1)
        EndIf

    Next

Return aLista


/*/{Protheus.doc} ListQuery
@description Monta array com lista de valores com base na Query informada no parametro

@author Helitom Silva
@since  13/02/2015

@param p_aQuery, Array, Array a Consultas SQLs
@param p_aCols, array, Campos que seram preenchidos com o Select.

@return aLista, Array, Array com valores para montar a grid de sele��o

/*/
Static Function ListQuery(p_aQuery, p_aCols)

    Local aRet      := {}
    Local nZ    	:= 0
    Local nI    	:= 0
    Local lMak      := .F.
    Local aCodItens := ListPar(AllTrim(cIDACGMC))

    Default p_aQuery := {}
    Default p_aCols  := {}

    For nZ := 1 To Len(p_aQuery)

        If Select("QRY") > 0
            QRY->(DbCloseArea())
        EndIf

        TcQuery p_aQuery[nZ] New Alias "QRY"
        DbSelectArea("QRY")
        QRY->(DbGotop())

        While !QRY->(Eof())

            aAdd( aRet, Array(Len(p_aCols) + 1) )
            aRet[len(aRet)][1] := Iif( Empty(cIDACGMC), oNo, Iif(aScan(aCodItens, {|X| AllTrim(&("QRY->" + p_aCols[1])) == X}) > 0, oOk, oNo))

            For nI := 1 To Len(p_aCols)
                aRet[len(aRet)][nI + 1] := &("QRY->" + p_aCols[nI])
            Next

            QRY->(DbSkip())
        EndDo

        QRY->(DbCloseArea())

    Next

    If Len(aRet) = 0
        aAdd( aRet, Array(Len(p_aCols) + 1) )
        aRet[1][1] := ""//.F.
        For nI := 1 To Len(p_aCols)
            aRet[1][nI + 1] := " "
        Next
    EndIf

Return aRet


/*/{Protheus.doc} PesqMarca
@description Consulta e marca linha conforme conte�do informado.

@author Helitom Silva
@since  12/02/2015

@param p_nPesq, Numerico, Numero da Coluna a ser pesquisada
@param p_cTexto, Caracter, Texto a ser pesquisado

/*/
Static Function PesqMarca(p_nPesq, p_cTexto)

    Local nAchou	:= .F.

    If Empty(oGridSel:aArray)
        Return
    EndIf

    If !Empty( nAchou := aScan( oGridSel:aArray , {|x| AllTrim(p_cTexto) $ Upper(x[p_nPesq + 1]) } ) )
        cTexto := Space(25)
        MarcaLinha(nAchou, .T.)
        oGridSel:nAt := nAchou
        oGridSel:Refresh()
        oGridSel:SetFocus()
    Else
        oGetText:SetFocus()
        MsgInfo(cUserName+Chr(13) + "Registro n�o Encontrado.")
    EndIf

Return


/*/{Protheus.doc} MarcaLinha
@description Marca e Desmarca a Linha ou todas

@author Helitom Silva
@since 12/02/2015

@param p_nAt, Numerico, Linha a ser marcada
@param p_lMarca, Logico, Se .T. Marca a linha caso contrario avalia e marca ou desmarca conforme o valor existente na marca��o
@param p_lMarcAll, Logico, Se .T. Marca todas as Linhas caso contrario desmarca todas as linhas

/*/
Static Function MarcaLinha(p_nAt, p_lMarca, p_lMarcAll)

    Local 	nI 	:= 0

    Default p_nAt	   := 0
    Default p_lMarca   := .F.
    Default p_lMarcAll := .F.

    If p_nAt = 0

        For nI := 1 to Len(oGridSel:aArray)
            If p_lMarcAll
                oGridSel:aArray[nI][1] := oOk
            Else
                oGridSel:aArray[nI][1] := oNo
            EndIf
        Next

    Else

        If ValType(oGridSel:aArray[p_nAt][1]) <> "C"

            If p_nAt > 0
                If p_lMarca
                    oGridSel:aArray[p_nAt][1] = oOk
                Else
                    If oGridSel:aArray[p_nAt][1] = oOk
                        oGridSel:aArray[p_nAt][1] := oNo
                    Else
                        oGridSel:aArray[p_nAt][1] = oOk
                    EndIf
                EndIf
            Else
                For nI := 1 to Len(oGridSel:aArray)
                    If p_lMarcAll
                        oGridSel:aArray[nI][1] := oOk
                    Else
                        oGridSel:aArray[nI][1] := oNo
                    EndIf
                Next
            EndIf
        Else
            oGridSel:aArray[p_nAt][1] := oOk
        EndIf
    EndIf

    oGridSel:Refresh()

Return


/*/{Protheus.doc} Confirma
@description Confirmaa��o do Formul�rio

@author Helitom Silva
@since  13/02/2015

@return lRet, Logico, Se tudo estiver OK retorna .T.

/*/
Static Function Confirma()

    Local lRet	 := .T.
    Local cConds := ''
    Local nY	 := 0

    For nY := 1 To Len(oGridSel:aArray)

        If oGridSel:aArray[nY][1] = oOk

            aIDACGMC := oGridSel:aArray[nY]

            If Empty(cConds)
                cConds += oGridSel:aArray[nY, 2]
            Else
                cConds += '|' + oGridSel:aArray[nY, 2]
            EndIf

        EndIf

    Next

    cIDACGMC := cConds

Return lRet