#INCLUDE 'TOTVS.CH'


/*/{Protheus.doc} IdaRelTm
@description Relat�rio padr�o para impressora t�rmica.
	
@author  Helitom Silva
@since   19/06/2015
@version 1.0

/*/
Class IdaRelTm From FWMSPrinter

    DATA dDateRel
    DATA cFunc
    DATA lUpper
    DATA oPrint
    DATA cTitulo
    DATA nCol
    DATA nWidth
    DATA nCurLine HIDDEN
    DATA oFont
    DATA cAten
    DATA cEmpRel
    DATA cFilRel
    DATA oEmpData

    Method New() CONSTRUCTOR
    Method PHeader() 	  /* Cabe�alho */
    Method PFooter()	  /* Rodap� */
    Method SetCurLine()
    Method GetCurLine()
    Method AddLine()
    Method SetCurCol()
    Method GetCurCol()
    Method AddCol()
    Method SetWidth()
    Method GetWidth()
    Method MsgEndRep()
    Method OpenPrinter()  /* Abre comunica��o com a impressora termica */
    Method ClosePrinter() /* Fecha comunica��o com a impressora termica */
    Method ColCenter()    /* Alinha Texto Centralizado */
    Method PLine()		  /* Imprime uma linha tracejada */

EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da Classe.

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_dData, Data, Data da Ocorrencia
@param p_cFunc, Caracter, Funcao
@param p_lUpper, Logico, Fonte Caixa Alta
@param p_cTitulo, Caracter, Descri��o do Titulo
@param p_oFont, Objeto, Fonte para impressao
@param p_lConsImp, Logico, Define se consulta o cadastro de impressoras para obter a impressora da esta��o.
@param p_cEmpRel, Logico, Empresa para impress�o do Cabe�alho.
@param p_cFilRel, Logico, Filial para impress�o do Cabe�alho.

/*/
Method New( p_dData, p_cFunc, p_lUpper, p_cTitulo, p_oFont, p_nCol, p_nWidth, p_lConsImp, p_cEmpRel, p_cFilRel) Class IdaRelTm

    Local cImpTerm		:= ''
    Local cImpSetup		:= ''
    Local aArrayImp		:= {}

    Default p_dData 	:= dDataBase
    Default p_cFunc 	:= ''
    Default p_lUpper 	:= .F.
    Default p_cTitulo	:= ''
    Default p_oFont		:= TFont():New( "Courier new",,-8,,.T. )
    Default p_nCol		:=	2
    Default p_nWidth	:= 200
    Default p_lConsImp	:= .F.
    Default p_cEmpRel	:= cEmpAnt
    Default p_cFilRel	:= cFilAnt

    Self:dDateRel		:= p_dData
    Self:cFunc  		:= p_cFunc
    Self:lUpper			:= p_lUpper
    Self:cTitulo     	:= p_cTitulo
    Self:oFont 			:= p_oFont
    Self:cEmpRel 		:= p_cEmpRel
    Self:cFilRel 		:= p_cFilRel
    Self:oEmpData 		:= IdaTable():New('SM0', .T., .F., " M0_CODIGO = '" + Self:cEmpRel + "' .AND. M0_CODFIL = '" + Self:cFilRel + "' ")

    Self:SetCurCol(p_nCol)
    Self:SetWidth(p_nWidth)
    Self:SetCurLine(0)

    If p_lConsImp .or. Type('cImpTermIda') == 'U' .or. Type('cImpSetuIda') == 'U'
        oImpAFP   := IDACEI():New( GetClientIP() )
        cImpTerm  := oImpAFP:CONSULTTHERMAL()
        cImpSetup := oImpAFP:CONSULTSETUP()
    ElseIf !Type('cImpTermIda') == 'U' .and. !Empty(AllTrim(cImpTermIda))
        cImpTerm := cImpTermIda /* Vari�vel publica definida atraves da Fun��o U_IDACEI02 executada no PE: AFTERLOGIN */
    Else
        If Len(aArrayImp := GetImpWindows(.F.)) > 0
            cImpTerm := aArrayImp[1]
        EndIf
    EndIf

    If !Type('cImpSetuIda') == 'U' .and. !Empty(AllTrim(cImpSetuIda))
        cImpSetup := cImpSetuIda /* Vari�vel publica definida atraves da Fun��o U_IDACEI02 executada no PE: AFTERLOGIN */
    EndIf

    _Super:New( "REL_" + AllTrim(FWUUIDV1()),, .F., GetNewPar('MV_RELT', '\spool\'), Iif(cImpSetup == '1', .T., .F.),, @Self, cImpTerm, .F., .F.,, )

Return Self


/*/{Protheus.doc} PHeader
@description Metodo que monta o cabe�alho do relatorio.

@author  Helitom Silva
@since   19/06/2015
@version 1.0

/*/
Method PHeader() Class IdaRelTm

    Local cImpRetCli := ""
    Local cIPClient	 := GetClientIP()
    Local cInIfile	 := GetADV97()
    Local cBitmap 	 := GetPvProfString( GetEnvServer(), "StartPath", "ERROR", cInIfile ) + "\lgrl" + SubStr(Self:oEmpData:GetValue('M0_CODIGO'), 1, 2) + ".bmp"

    Local aFone1	 := StrTokArr( Replace( Replace( Replace( Self:oEmpData:GetValue('M0_TEL'), "-","" ),"(","") , ")","" ) , "," )
    Local cFone		 := ""

    Local nI		 := 0

    For nI := 1 to len(aFone1)
        cFone +=  "  (" + SubStr(AllTrim(aFone1[nI]),1,2) + ")" + SubStr(AllTrim(aFone1[nI]),3,4) + '-' + SubStr(AllTrim(aFone1[nI]),7,4)
    Next

    //Self:SetPaperSize(0, 1800, 300)
    Self:SetPortrait()
    Self:StartPage()

    Self:SetCurCol(2)

    Self:SayBitmap( Self:GetCurLine(), Self:GetCurCol(), cBitmap, 160, 50 )

    Self:AddLine(8)
    Self:SayAlign( Self:GetCurLine(), Self:GetCurCol(), SubStr( Upper(FWFilialName(Self:cEmpRel, Self:cFilRel)), 1, 40 ), Self:oFont, Self:nWidth, , , 0 )

    Self:AddLine()

    Self:SayAlign(  Self:AddLine(), Self:GetCurCol(), "Endere�o: " + SubStr( Self:oEmpData:GetValue('M0_ENDENT'), 1, 35 ), Self:oFont, Self:nWidth, , , 0 )

    Self:SayAlign( Self:AddLine(), Self:GetCurCol(), "Bairro: " + Self:oEmpData:GetValue('M0_BAIRENT'), Self:oFont, Self:nWidth, , , 0 )

    Self:SayAlign( Self:AddLine(), Self:GetCurCol(), "Munic�pio: " + RTrim(Self:oEmpData:GetValue('M0_CIDENT')) + " - UF: " + Upper(Self:oEmpData:GetValue('M0_ESTENT')), Self:oFont, Self:nWidth, , , 0 )

    Self:SayAlign( Self:AddLine(), Self:GetCurCol(), "Fone: " + cFone , Self:oFont, Self:nWidth, , , 0 )

    Self:SayAlign( Self:AddLine(), Self:GetCurCol(), "Data: " + DtoC(Self:dDateRel) , Self:oFont, Self:nWidth, , , 0 )

    Self:AddLine()

    //Self:SayAlign( Self:AddLine(0.8), Self:GetCurCol(), cSpace, , Self:GetWidth(), , , 2 ) /* Linha */
    Self:PLine(Self:AddLine(0.8), Self:GetCurCol(), Self:GetWidth())

Return


/*/{Protheus.doc} PFooter
@description Monta o PFooter do relatorio.

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_nLin, Numerico, (Posi��o da linha que ser� impressa)

/*/
Method PFooter( p_nLin ) Class IdaRelTm

    Default p_nLin := Self:GetCurLine()

    p_nLin := p_nLin + 10

    Self:Say( p_nLin, Self:GetCurCol(), "", Self:oFont )

Return


/*/{Protheus.doc} SetCurLine
@description Define numero da linha atual

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_nLin, Numerico, (Informa o numero da linha atual)

/*/
Method SetCurLine(p_nLin) Class IdaRelTm

    Default p_nLin := 0

    Self:nCurLine := p_nLin

Return

/*/{Protheus.doc} GetCurLine
@description Metodo que retorna a linha atual

@author  Helitom Silva
@since   19/06/2015
@version 1.0

/*/
Method GetCurLine() Class IdaRelTm

    Local nLin := 0

    nLin := Self:nCurLine

Return nLin


/*/{Protheus.doc} AddLine
@description Metodo que adiciona(Salta) linha

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_nAddLin, Numerico, Quantidade de linha pra ser incrementado, se n�o informado incrementa uma linha

/*/
Method AddLine( p_nAddLin ) Class IdaRelTm

    Local nNewLine := 0

    Default p_nAddLin := 1

    nNewLine := Self:GetCurLine() + (p_nAddLin * 10)

    Self:SetCurLine(nNewLine)

	/*Local rNumLin := 0  

	Private nPrLin := 48   //Quantidade de Pixel para Proxima Linha de Caracteres
	Private nPxDepLe := 55 //Quantidade de Pixel para Proxima Linha Apos Linha de Caracteres
	Private nPxDepL := 14  //Quantidade de Pixel para Proxima Linha de Caracteres Apos Linha
		
    If pTpLin = 1
	   rNumLin := pLin + (nPrLin * pQtdLin)
    Endif

    If pTpLin = 2
	   rNumLin := pLin + (nPxDepLe * pQtdLin)
    Endif
	
    If pTpLin = 3
	   rNumLin := pLin + (nPxDepL * pQtdLin)
    Endif*/
	
Return nNewLine


/*/{Protheus.doc} SetCurCol
@description Define numero da coluna corrente

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_nLin, Numerico, Informa o numero da linha atual

/*/
Method SetCurCol(p_nCol) Class IdaRelTm

    Default p_nCol := 2

    Self:nCol := p_nCol

Return Self:GetCurCol()


/*/{Protheus.doc} GetCurCol
@description Retorna numero da Coluna Corrente

@author  Helitom Silva
@since   19/06/2015
@version 1.0

/*/
Method GetCurCol() Class IdaRelTm
Return Self:nCol


/*/{Protheus.doc} AddCol
@description Metodo que adiciona(Salta) coluna

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_nAddCol, Numerico, Quantidade de colunas para ser incrementada

/*/
Method AddCol(p_nAddCol) Class IdaRelTm

    Local nNewCol := 0

    Default p_nAddCol := 0

    Self:SetCurCol(Self:nCol + p_nAddCol)

Return Self:GetCurCol()


/*/{Protheus.doc} SetWidth
@description Seta o numero da largura do relat�rio

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@param p_nLin, Numerico, Informa o numero da largura atual

/*/
Method SetWidth(p_nWidth) Class IdaRelTm

    Default p_nWidth := 0

    Self:nWidth := p_nWidth

Return


/*/{Protheus.doc} GetWidth
@description Metodo que retorna a largura relat�rio

@author  Helitom Silva
@since   19/06/2015
@version 1.0

/*/
Method GetWidth() Class IdaRelTm
Return Self:nWidth


/*/{Protheus.doc} MsgEndRep
@description Imprimi mensagem no fim do relatorio.

@author  Helitom Silva
@since   19/06/2015
@version 1.0

/*/
Method MsgEndRep(p_cMensagem, p_lUpperMot, p_nLin, p_nWidth) Class IdaRelTm

    Default p_lUpperMot	:= .F.
    Default p_cMensagem	:= ' '

    If !Empty(p_lUpperMot)
        Self:SayAlign( p_nLin, 10, Upper(p_cMensagem), Self:oFont, p_nWidth, , , 0 )
    Else
        Self:SayAlign( p_nLin, 10, p_cMensagem, Self:oFont, p_nWidth, , , 0 )
    EndIf

    Self:SayAlign( p_nLin, 50, ": " + p_cMensagem, Self:oFont, p_nWidth, , , 0 )

    Self:SetCurLine(p_nLin)

Return


/*/{Protheus.doc} OpenPrinter
@description Abre a porta de comunica��o da impressora

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@return nHdlECF, Codigo retornado da referencia da comunica��o com a porta.

/*/
Method OpenPrinter() Class IdaRelTm

    If nHdlECF == -1
        cModelo	:= LJGetStation("IMPFISC")
        cPorta	:= LJGetStation("PORTIf")
        nHdlECF := INFAbrir( cModelo, cPorta )
    EndIf

Return nHdlECF


/*/{Protheus.doc} ClosePrinter
@description Fecha a porta de comunica��o da impressora

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@return nHdlECF, Codigo retornado da referencia da comunica��o com a porta.
/*/
Method ClosePrinter() Class IdaRelTm

    If nHdlECF == 0
        nHdlECF := InfFechar()
    EndIf

Return nHdlECF


/*/{Protheus.doc} ColCenter
@description Fecha a porta de comunica��o da impressora

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@return nColCen, Caracter, Retorna a coluuna.
/*/
Method ColCenter(p_Text) Class IdaRelTm

    Local nColCen := 2
    Local nTamLet := 5
    Local nQtdLet := 40 /* Quantidade letras por linha */

    Default p_Text := ""

    nColCen := (( nQtdLet - Len(p_Text) )/2) * nTamLet

Return nColCen


/*/{Protheus.doc} PLine
@description Imprime linha tracejada

@author  Helitom Silva
@since   19/06/2015
@version 1.0

@return p_nLine, Numeric, Numero da Linha.
@return p_nColumn, Numeric, Numero da Coluna.
@return p_nWidth, Numeric, Largura da Linha.

/*/
Method PLine(p_nLine, p_nColumn, p_nWidth) Class IdaRelTm

    Default p_nLine   := Self:GetCurLine()
    Default p_nColumn := Self:GetCurCol()
    Default p_nWidth  := Self:GetWidth()

    Self:SayAlign( p_nLine, p_nColumn, Replicate("-", p_nWidth), , p_nWidth, , , 2 )

Return