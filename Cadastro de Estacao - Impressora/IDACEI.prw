#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "IDATOOLS.CH"
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'


/*/{Protheus.doc} IdaCEI
@description Cadastro de Esta��o para impressoras.
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0	

/*/
User Function IDACEI()

    Local oEstImp	   := IdaCEI():New()

    Private cAliasCEI  := oEstImp:GetTable()
    Private cIpEst	   := GetClientIP()
    Private cModel	   := cAliasCEI + '_MODEL'
    Private cFunction  := 'IdaCEI'
    Private cTela	   := 'Esta��o'
    Private oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias( cAliasCEI )
    oBrowse:SetDescription( 'Cadastro de Esta��o' )
    oBrowse:SetFilterDefault("" + cAliasCEI + "->" + cAliasCEI + "_FILIAL == xFilial('" + cAliasCEI + "') .AND. " + cAliasCEI + "->" + cAliasCEI + "_CODIGO = cIpEst")

    oBrowse:Activate()

Return


/*/{Protheus.doc} MenuDef
@description Retorna os itens para constru��o do menu da rotina
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0

@return aRotina, Array, Lista com os itens do menu

/*/
Static Function MenuDef()

    Local aRotina := {}

    aAdd( aRotina, { "Visualizar", "ViewDef.IdaCEI", 0, 2, 0, Nil } )
    aAdd( aRotina, { "Incluir"   , "U_IDACEI01(3)", 0, 3, 0, Nil } )
    aAdd( aRotina, { "Alterar"   , "U_IDACEI01(4)", 0, 4, 0, Nil } )
    aAdd( aRotina, { "Excluir"   , "ViewDef.IdaCEI", 0, 5, 0, Nil } )

Return aRotina


/*/{Protheus.doc} ModelDef
@description Retorna o modelo padrao para a rotina
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0
@return oModel, Objeto, Modelo de dados

/*/
Static Function ModelDef()

    Local oStruCEI := FWFormStruct( 1, cAliasCEI)
    Local oModel

    oModel := MPFormModel():New( 'UIDACEI')
    oModel:AddFields( cModel, , oStruCEI )

Return oModel


/*/{Protheus.doc} ViewDef
@description Retorna os View do modelo de dados
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0

@return oView, Logico, View do modelo de dados

/*/
Static Function ViewDef()

    Local oModel	:= FWLoadModel( "IDACEI" )
    Local oStruCEI	:= FWFormStruct( 2, cAliasCEI )
    Local oView

    oView := FWFormView():New()

    oView:SetModel( oModel )
    oView:AddField( cModel, oStruCEI)
    oView:CreateHorizontalBox( "CENTER" , 50 )
    oView:SetOwnerView( cModel, "CENTER" )

Return oView


/*/{Protheus.doc} UIdaCEI
@description Ponto de Entrada Geral da Rotina
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0
		
@param PARAMIXB, Array, [Objeto][IdPonto][IdModel]

@return uRet,  Retorno diversos do PE

/*/
User Function UIDACEI()

    Local uRet		 := .T.
    Local oObj       := ParamIXB[1]
    Local cIdPonto   := ParamIXB[2]
    Local cIdModel   := ParamIXB[3]

    Local lIsGrid    := .F.
    Local nX		 := 0
    Local nLinha     := 0
    Local nQtdLinhas := 0
    Local aAceite	 := {}
    Local aDadosChq	 := {}
    Local cBanco     := ''
    Local cAgenc   	 := ''
    Local cConta     := ''
    Local cNumChq    := ''

    If (lIsGrid := ( oObj:ClassName() == 'FWFORMGRID' ))

        nQtdLinhas := oObj:GetQtdLine()
        nLinha     := oObj:nLine

    EndIf

    If cIdPonto == 'MODELPOS' /* Chamada na valida��o total do modelo. */
    ElseIf cIdPonto == 'FORMPOS' /* Chamada na valida��o total do formul�rio. */
    ElseIf cIdPonto == 'FORMLINEPRE' /* Chamada na pre valida��o da linha do formul�rio. */
    ElseIf cIdPonto == 'FORMLINEPOS' /* Chamada na valida��o da linha do formul�rio. */
    ElseIf cIdPonto == 'MODELVLDACTIVE' /* Chamada na valida��o da ativa��o do Modelo. */
    ElseIf cIdPonto == 'MODELCOMMITTTS' /* Chamada apos a grava��o total do modelo e dentro da transa��o. */
    ElseIf cIdPonto == 'MODELCOMMITNTTS' /* Chamada apos a grava��o total do modelo e fora da transa��o. */
    ElseIf cIdPonto == 'FORMCOMMITTTSPOS' /* Chamada apos a grava��o da tabela do formul�rio. */

		/* Re-define Vari�veis Publicas de Impressoras */
        U_IDACEI02()

    ElseIf cIdPonto == 'MODELCANCEL' /* Cancela */
        uRet := .T.
    ElseIf cIdPonto == 'BUTTONBAR' /* Usado para Cria��o de Botoes Estrutura: { {'Nome', 'Imagem Botap', { || bBlock } } } */
    EndIf

Return uRet


/*/{Protheus.doc} IdaCEI
@description Consultar impressora do cadastro de esta��o.	

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
    Class IdaCEI

        Data cPrintSel 	As String
        Data cIPClient	As String
        Data cPsqSel	As String
        Data cTable		As String
        Data cAliasCEI  As String

        Method New() CONSTRUCTOR
        Method GetTable()		/* Consultar Tabela usada pelo Cadastro de Impressora */
        Method InfTable()   	/* Formul�rio para informar a tabela a ser usada pelo Cadastro de Esta��o e Impressora */
        Method ConsultThermal()	/* Consultar Impressora Termica */
        Method ConsultLaser()	/* Consultar Impressora Laser */
        Method ConsultSetup()	/* Consultar Se Exibe Setup de Impress�o */

    EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da Classe

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Method New( cIPParam ) Class IdaCEI

    Default cIPParam := GetClientIP()

    Self:cPrintSel	:= ""
    Self:cPsqSel	:= ""
    Self:cTable		:= Self:GetTable()
    Self:cAliasCEI	:= Self:GetTable()

    If Empty( cIPParam )
        Self:cIPClient	:= cIPParam
    Else
        Self:cIPClient	:= cIPParam
    EndIf

Return Self


/*/{Protheus.doc} GetTable
@description Retorna Alias da Tabela usada pelo Cadastro de Esta��o

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Method GetTable() Class IdaCEI

    If Empty( AllTrim(Self:cTable) )

        Self:cTable	:= GetNewPar('IDA_CADEST', '')

        While Empty( AllTrim(Self:cTable) )

            Self:cTable	:= Self:InfTable()

            If Empty( AllTrim(Self:cTable) )
                MsgAlert('N�o foi informado tabela do cadastro de Esta��o - Impressora, por favor, verifique seu dicion�rio de dados e informe um nome Vago!')
            Else
                IdaPutMv('IDA_CADEST', Self:cTable, '', 'Tabela do cadastro de Esta��o - Impressora')
            EndIf

        End

    EndIf

Return Self:cTable


/*/{Protheus.doc} InfTable
@description Formul�rio para informar a tabela de dados.
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0
		
@return cRet, Caracter, Tabela de dados.

/*/
Method InfTable() Class IdaCEI

    Local cRet := ''

	/* Declara��o de cVariable dos componentes */
    Private cMGetInfo := 'Informe a tabela que ser� usada para o cadastro de Esta��o - Impressora'
    Private cGetTab   := Space(3)

	/* Declara��o de Variaveis Private dos Objetos */
    SetPrvt("oDlgPerg", "oSayTab", "oGetTab", "oMGetInfo", "oSBtnOk")

	/* Definicao do Dialog e todos os seus componentes. */
    oDlgPerg  := MSDialog():New( 092, 222, 184, 560, "Cadastro Esta��o - Impressora",,,.F.,,,,,,.T.,,,.T. )
    oMGetInfo := TMultiGet():New( 002, 002, {|u| If(PCount() > 0, cMGetInfo := u, cMGetInfo)}, oDlgPerg, 166, 028,,,CLR_BLACK,CLR_WHITE,,.T.,"",, {|| .F.},.F.,.F.,.F.,,,.F.,,  )
    oSayTab   := TSay():New( 036, 002, {|| "Tabela"}, oDlgPerg,,,.F.,.F.,.F.,.T., CLR_BLACK, CLR_WHITE, 020, 012)
    oGetTab   := TGet():New( 035, 027, {|u| If(PCount() > 0, cGetTab := u, cGetTab)}, oDlgPerg, 020, 008,'@!',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","",,)
    oSBtnOk   := SButton():New( 034, 142,1, {|| cRet := cGetTab, oDlgPerg:End()}, oDlgPerg,,"", )

    oDlgPerg:Activate(,,, .T.)

Return cRet


/*/{Protheus.doc} ConsultThermal
@description Retorna Impressora t�rmica

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Method ConsultThermal() Class IdaCEI

    Local cAliasTMP := GetNextAlias()
    Local cQuery    := ''

    Iif(Select(cAliasTMP) > 0, (cAliasTMP)->(DbCloseArea()), Nil)

    cQuery := "	SELECT " + Self:cAliasCEI + "." + Self:cAliasCEI + "_IMP2 AS IMP2 "
    cQuery += "   FROM " + RetSQLName(Self:cAliasCEI) + " " + Self:cAliasCEI + " "
    cQuery += "	 WHERE " + Self:cAliasCEI + "." + Self:cAliasCEI + "_CODIGO = '" + Self:cIPClient + "' "
    cQuery += "	   AND " + Self:cAliasCEI + "." + Self:cAliasCEI + "_FILIAL = '" + xFilial(Self:cAliasCEI) + "' "
    cQuery += "	   AND " + Self:cAliasCEI + ".D_E_L_E_T_ = ' ' "

    TcQuery cQuery NEW ALIAS (cAliasTMP)

    DbSelectArea(cAliasTMP)

    If !(cAliasTMP)->(Eof())
        Self:cPrintSel := RTrim((cAliasTMP)->IMP2)
    EndIf

    Iif(Select(cAliasTMP) > 0, (cAliasTMP)->(DbCloseArea()), Nil)

Return Self:cPrintSel


/*/{Protheus.doc} ConsultLaser
@description Retorna Impressora Laser

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Method ConsultLaser() Class IdaCEI

    Local cAliasTMP := GetNextAlias()
    Local cQuery    := ''

    Iif(Select(cAliasTMP) > 0, (cAliasTMP)->(DbCloseArea()), Nil)

    cQuery := "	SELECT " + Self:cAliasCEI + "." + Self:cAliasCEI + "_IMP1 AS IMP1 "
    cQuery += "   FROM " + RetSQLName(Self:cAliasCEI) + " " + Self:cAliasCEI + " "
    cQuery += "	 WHERE " + Self:cAliasCEI + "." + Self:cAliasCEI + "_CODIGO = '" + Self:cIPClient + "' "
    cQuery += "	   AND " + Self:cAliasCEI + "." + Self:cAliasCEI + "_FILIAL = '" + xFilial(Self:cAliasCEI) + "' "
    cQuery += "	   AND " + Self:cAliasCEI + ".D_E_L_E_T_ = ' ' "

    TcQuery cQuery NEW ALIAS (cAliasTMP)

    DbSelectArea(cAliasTMP)

    If !(cAliasTMP)->(Eof())
        Self:cPrintSel := RTrim((cAliasTMP)->IMP1)
    EndIf

    Iif(Select(cAliasTMP) > 0, (cAliasTMP)->(DbCloseArea()), Nil)

Return Self:cPrintSel


/*/{Protheus.doc} ConsultSetup
@description Retorna se usa pesquisa de satisfa��o

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Method ConsultSetup() Class IdaCEI

    Local cAliasTMP := GetNextAlias()
    Local cQuery    := ''

    Iif(Select(cAliasTMP) > 0, (cAliasTMP)->(DbCloseArea()), Nil)

    cQuery := "	SELECT " + Self:cAliasCEI + "." + Self:cAliasCEI + "_PSQSTF AS PSQSTF "
    cQuery += "   FROM " + RetSQLName(Self:cAliasCEI) + " " + Self:cAliasCEI + " "
    cQuery += "	 WHERE " + Self:cAliasCEI + "." + Self:cAliasCEI + "_CODIGO = '" + Self:cIPClient + "' "
    cQuery += "	   AND " + Self:cAliasCEI + "." + Self:cAliasCEI + "_FILIAL = '" + xFilial(Self:cAliasCEI) + "' "
    cQuery += "	   AND " + Self:cAliasCEI + ".D_E_L_E_T_ = ' ' "

    TcQuery cQuery NEW ALIAS (cAliasTMP)

    If !(cAliasTMP)->(Eof())
        Self:cPsqSel := RTrim((cAliasTMP)->PSQSTF)
    EndIf

    Iif(Select(cAliasTMP) > 0, (cAliasTMP)->(DbCloseArea()), Nil)

Return Self:cPsqSel