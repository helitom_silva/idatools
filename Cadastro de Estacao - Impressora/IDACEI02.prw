#INCLUDE 'PROTHEUS.CH'


/*/{Protheus.doc} IDACEI02
@description Define Vari�veis Publicas de Impressoras

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
User Function IDACEI02()

    Local cIPClient	:= GetClientIP()
    Local oImpAFP	:= IDACEI():New( cIPClient ) /* Consultar Impressora no cadastro de Esta��o. */

    If Type('cImpTermIda') == 'U'
        Public cImpTermIda	:= oImpAFP:CONSULTTHERMAL()
    Else
        cImpTermIda	:= oImpAFP:CONSULTTHERMAL()
    EndIf

    If Type('cImpLaseIda') == 'U'
        Public cImpLaseIda := oImpAFP:CONSULTLASER()
    Else
        cImpLaseIda	:= oImpAFP:CONSULTLASER()
    EndIf

    If Type('cImpSetuIda') == 'U'
        Public cImpSetuIda := oImpAFP:CONSULTSETUP()
    Else
        cImpSetuIda	:= oImpAFP:CONSULTSETUP()
    EndIf

Return