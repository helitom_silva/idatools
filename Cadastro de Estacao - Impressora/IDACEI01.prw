#INCLUDE 'PROTHEUS.CH'


/*/{Protheus.doc} IDACEI01
@description Selecionar as impressoras da esta��o. Impressora Laser e Termica.
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0	

/*/
User Function IDACEI01(p_nOpc)

    Local cDescDlg		:= "Cadastro de Esta��o"
    Local cTST			:= ""

    Private cLibEst		:= CriaVar(cAliasCEI + "_PSQSTF")
    Private cIpEst		:= CriaVar(cAliasCEI + "_CODIGO")
    Private cImpLaser	:= CriaVar(cAliasCEI + "_IMP1")
    Private cImpTerm	:= CriaVar(cAliasCEI + "_IMP2")
    Private aArrayImp	:= GetImpWindows(.F.)

    SetPrvt("oWindow","oGet","oGet1","oGet2","oBtn","oBtn1","oSBtn1","oSBtn2","oGrp","oGrp1","oGrp2", "oBtnSave", "oBtnExit")

    If CheckReg()
        Return .T.
    EndIf

    oWindow		:= MSDialog():New(180,180,500,522,cDescDlg,,,.F.,,,,,,.T.,,,.T.)

    oGrp		:= TGroup():New(004,004,030,170, "IP Esta��o", oWindow,CLR_BLACK,CLR_WHITE,.T.,.F.)
    oGet		:= TGet():New( 15,15,{|u| If(PCount()>0, cIpEst:= u, cIpEst)},oGrp,100,008,"@!",,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","",,)
    oGet:bWhen	:= {|| .F.}

    oGrp1		:= TGroup():New(040,004,066,170, "Impressora Laser",	oWindow,CLR_BLACK,CLR_WHITE,.T.,.F.)
    oCombBox1	:= TComboBox():New( 051,015, { |u| If( PCount() > 0, cImpLaser := u, cImpLaser ) }, aArrayImp,140,010,oGrp1,,,,,,.T.,,,,,,,,,'cImpLaser')

    oGrp2		:= TGroup():New( 076, 004, 102, 170, "Impressora Termica", oWindow, CLR_BLACK, CLR_WHITE, .T., .F. )
    oCombBox2	:= TComboBox():New( 087, 015, { |u| If( PCount() > 0, cImpTerm := u, cImpTerm ) }, aArrayImp, 140, 010, oGrp2,,,,,, .T.,,,,,,,,, 'cImpTerm')

    oGrp3		:= TGroup():New(107,004,130,170, "Exibe Setup?", oWindow,CLR_BLACK,CLR_WHITE,.T.,.F.)
    oCombBox3	:= TComboBox():New( 115, 015, { |u| If( PCount() > 0, cLibEst := u, cLibEst ) }, {"1=N�o","2=Sim"}, 40, 010, oGrp3,,,,,, .T.,,,,,,,,, 'cLibEst')
    oCombBox3:bWhen	:= {|| .T.}

    If p_nOpc = 6
        oCombBox3:bWhen	:= {|| .T.}
    EndIf

    If Altera
        oGet:bWhen := {|| .F.}
        cIpEst	   := &("" + cAliasCEI + "->" + cAliasCEI + "_CODIGO")
        cImpLaser  := &("" + cAliasCEI + "->" + cAliasCEI + "_IMP1")
        cImpTerm   := &("" + cAliasCEI + "->" + cAliasCEI + "_IMP2")
        cLibEst	   := &("" + cAliasCEI + "->" + cAliasCEI + "_PSQSTF")
    EndIf

    oBtnSave := TButton():New( 135,050, "&Salvar", oWindow, {|| SalvarEst(), oWindow:End() },030,015,,,,.T.,,"",,,,.F. )
    oBtnExit := TButton():New( 135,090, "&Fechar", oWindow, {|| oWindow:End() }, 030,015,,,,.T.,,"",,,,.F. )

    oWindow:Activate(,,,.T.,,,/**/)

Return .T.


/*/{Protheus.doc} CheckReg
@description Verifica se a esta��o j� est� cadastrada.

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Static Function CheckReg()

    Local cMsgInfInc	:= "Esta��o j� est� Cadastrada!"
    Local cMsgInfAlt	:= "O IP n�o corresponde a essa esta��o!"
    Local lRet			:= .F.

    If Inclui
        If dbSeek( xFilial(cAliasCEI) + cIpEst )
            MsgAlert(cMsgInfInc)
            lRet := .T.
        EndIf
    EndIf

    If Altera
        If cIpEst <> &("" + cAliasCEI + "->" + cAliasCEI + "_CODIGO")
            MsgAlert(cMsgInfAlt)
            lRet := .T.
        EndIf
    EndIf

Return lRet


/*/{Protheus.doc} SalvarEst
@description Inclui um novo registro ou Altera o registro posicionado.

@author  Helitom Silva
@since   18/06/2015
@version 1.0

/*/
Static Function SalvarEst()

    If Inclui

        If RecLock(cAliasCEI, .T.)
            &("" + cAliasCEI + "->" + cAliasCEI + "_FILIAL") := xFilial(cAliasCEI)
            &("" + cAliasCEI + "->" + cAliasCEI + "_CODIGO") := cIpEst
            &("" + cAliasCEI + "->" + cAliasCEI + "_IMP1")   := cImpLaser
            &("" + cAliasCEI + "->" + cAliasCEI + "_IMP2")   := cImpTerm
            &("" + cAliasCEI + "->" + cAliasCEI + "_PSQSTF") := cLibEst
            (cAliasCEI)->(MsUnLock())
        EndIf

    ElseIf Altera

        If RecLock(cAliasCEI, .F.)
            &("" + cAliasCEI + "->" + cAliasCEI + "_IMP1")   := cImpLaser
            &("" + cAliasCEI + "->" + cAliasCEI + "_IMP2")   := cImpTerm
            &("" + cAliasCEI + "->" + cAliasCEI + "_PSQSTF") := cLibEst
            (cAliasCEI)->(MsUnLock())
        EndIf

    EndIf

	/* Define Vari�veis Publicas de Impressoras */
    U_IDACEI02()

Return