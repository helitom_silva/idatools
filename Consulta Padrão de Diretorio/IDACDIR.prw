#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'PARMTYPE.CH'


/*/{Protheus.doc} IDACDIR
@description Consulta Pad�o de Diret�rio

@author  Helitom Silva
@since   26/08/2016
@version 1.0

/*/
User Function IDACDIR()

    Local lRet := .T.

    Public __cIDACDIR

    __cIDACDIR := cGetFile('Csv |*.txt|', 'Textos (TXT)', 1, 'C:\', .F., nOR( GETF_LOCALHARD, GETF_LOCALFLOPPY, GETF_RETDIRECTORY ),.T., .T. )

    lRet := !Empty(__cIDACDIR)

Return lRet