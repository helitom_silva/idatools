#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'IDATOOLS.CH'
#INCLUDE 'IDAEXP.CH'

#DEFINE CR Chr(13) + Chr(10)

/*/{Protheus.doc} IDAEXP
@description Cadastro de SQL Exporta��o

@author  Helitom Silva
@since   23/08/2016
@version 1.0

/*/
User Function IDAEXP()

    Local oBrowse
    Local cAlias 	:= TBZ11

    SetPrvt('oGridSQL')

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias(TBZ11)
    oBrowse:SetDescription('Cadastro de SQL Exporta��o')
    oBrowse:AddLegend(Z11MSBLQL + " == '1' ", "BR_VERMELHO", "Bloqueado" )
    oBrowse:AddLegend(Z11MSBLQL + " == '2' ", "BR_BRANCO", "Desbloqueado" )
    oBrowse:DisableDetails()
    oBrowse:Activate()

Return


/*/{Protheus.doc} MenuDef
@description Funcao de definicao do Menu

@author Helitom Silva
@since 	23/08/2016

/*/
Static Function MenuDef()

    Local aRotina := {}

    ADD OPTION aRotina TITLE 'Pesquisar'  		   ACTION 'PesqBrw'        OPERATION 1 ACCESS 0
    ADD OPTION aRotina TITLE 'Visualizar' 		   ACTION 'VIEWDEF.IDAEXP' OPERATION 2 ACCESS 0
    ADD OPTION aRotina TITLE 'Incluir'    		   ACTION 'VIEWDEF.IDAEXP' OPERATION 3 ACCESS 0
    ADD OPTION aRotina TITLE 'Alterar'    		   ACTION 'VIEWDEF.IDAEXP' OPERATION 4 ACCESS 0
    ADD OPTION aRotina TITLE 'Excluir'    		   ACTION 'VIEWDEF.IDAEXP' OPERATION 5 ACCESS 0
    ADD OPTION aRotina TITLE 'Imprimir'   		   ACTION 'VIEWDEF.IDAEXP' OPERATION 8 ACCESS 0
    ADD OPTION aRotina TITLE 'Copiar'     		   ACTION 'VIEWDEF.IDAEXP' OPERATION 9 ACCESS 0
    ADD OPTION aRotina TITLE 'Executar Exporta��o' ACTION 'FwMsgRun(, {|lEnd| U_IDAEXPAG()}, "Exporta��o", "Executando Exporta��o dos dados SQL..." )' OPERATION 4 ACCESS 0

Return aRotina


/*/{Protheus.doc} ModelDef
@description Definicao do Model

@author Helitom Silva
@since 	23/08/2016

/*/
Static Function ModelDef()

    Local oStrTBZ11 := FWFormStruct(1, TBZ11)
    Local oModel   	:= Nil

    oModel := MPFormModel():New("MIDAEXP", /*PreValid*/, /*Valid*/, /*Grava*/ )

    oModel:AddFields("TBZ11MASTER", /*cOwner*/, oStrTBZ11, /*Pre-Validacao*/, /*Pos-Validacao*/)

    oModel:GetModel("TBZ11MASTER"):SetDescription("Cadastro de SQL Exporta��o")

    oModel:SetPrimaryKey({Z11FILIAL, Z11CODIGO})

Return oModel


/*/{Protheus.doc} ViewDef
@description Definicao da View

@author Helitom Silva
@since 	16/06/2015

/*/
Static Function ViewDef()

    Local oModel := FWLoadModel('IDAEXP')

    Local oStrTBZ11a := FWFormStruct(2, TBZ11)
    Local oStrTBZ11b := FWFormStruct(2, TBZ11)
    Local oView

    oStrTBZ11a:RemoveField( Z11SQL )

    oStrTBZ11b:RemoveField( Z11CODIGO )
    oStrTBZ11b:RemoveField( Z11GRUPO )
    oStrTBZ11b:RemoveField( Z11DESC )
    oStrTBZ11b:RemoveField( Z11MSBLQL )
    oStrTBZ11b:RemoveField( Z11DIR )
    oStrTBZ11b:RemoveField( Z11FILE )
    oStrTBZ11b:RemoveField( Z11TPFILE )
    oStrTBZ11b:RemoveField( Z11DELCSV )
    oStrTBZ11b:RemoveField( Z11TFIELD )
    oStrTBZ11b:RemoveField( Z11HREXP )
    oStrTBZ11b:RemoveField( Z11DTEXP )

    oStrTBZ11a:SetProperty( Z11CODIGO, MVC_VIEW_GROUP_NUMBER, 'CADASTRO1' )
    oStrTBZ11a:SetProperty( Z11GRUPO, MVC_VIEW_GROUP_NUMBER, 'CADASTRO1' )
    oStrTBZ11a:SetProperty( Z11DESC, MVC_VIEW_GROUP_NUMBER, 'CADASTRO1' )
    oStrTBZ11a:SetProperty( Z11MSBLQL, MVC_VIEW_GROUP_NUMBER, 'CADASTRO1' )

    oStrTBZ11a:SetProperty( Z11DIR, MVC_VIEW_GROUP_NUMBER, 'CADASTRO2' )
    oStrTBZ11a:SetProperty( Z11FILE, MVC_VIEW_GROUP_NUMBER, 'CADASTRO2' )
    oStrTBZ11a:SetProperty( Z11TPFILE, MVC_VIEW_GROUP_NUMBER, 'CADASTRO2' )
    oStrTBZ11a:SetProperty( Z11DELCSV, MVC_VIEW_GROUP_NUMBER, 'CADASTRO2' )
    oStrTBZ11a:SetProperty( Z11TFIELD, MVC_VIEW_GROUP_NUMBER, 'CADASTRO2' )

    oStrTBZ11a:SetProperty( Z11HREXP, MVC_VIEW_GROUP_NUMBER, 'CADASTRO3' )
    oStrTBZ11a:SetProperty( Z11DTEXP, MVC_VIEW_GROUP_NUMBER, 'CADASTRO3' )

    oStrTBZ11b:SetProperty( Z11SQL, MVC_VIEW_TITULO , '')

    oView := FWFormView():New()

    oView:SetModel(oModel)

    oView:addField( 'VIEW_TBZ11a', oStrTBZ11a, 'TBZ11MASTER' )
    oView:addField( 'VIEW_TBZ11b', oStrTBZ11b, 'TBZ11MASTER' )

    oView:CreateHorizontalBox( "SUPERIOR", 50)

    oView:CreateFolder( 'FOLDER', 'SUPERIOR')

    oView:AddSheet('FOLDER', 'SCADASTRO', 'Cadastro')
    oView:AddSheet('FOLDER', 'SSQL', 'SQL')

    oView:CreateHorizontalBox( 'CADASTRO', 100, , , 'FOLDER', 'SCADASTRO')

    oView:CreateVerticalBox( 'SQL', 70, , , 'FOLDER', 'SSQL')
    oView:CreateVerticalBox( 'BUTSQL', 30, , , 'FOLDER', 'SSQL')

    oView:CreateHorizontalBox( "GRIDRSQL", 50 )

    oView:AddOtherObject("PANELBUTT", {|oPanel| DefButtons(oPanel, .T.)})
    oView:AddOtherObject("PANELGRID", {|oPanel| DefGrid(oPanel, .T.)})

    oView:SetOwnerView( "VIEW_TBZ11a", 'CADASTRO' )
    oView:SetOwnerView( "VIEW_TBZ11b", 'SQL' )
    oView:SetOwnerView( 'PANELBUTT'  , 'BUTSQL' )
    oView:SetOwnerView( 'PANELGRID'  , 'GRIDRSQL' )

    //oView:EnableTitleView("VIEW_TBZ11a", "Cadastro")
    //oView:EnableTitleView("VIEW_TBZ11b", "Consulta")
    oView:EnableTitleView("PANELBUTT", "Op��es")
    oView:EnableTitleView("PANELGRID", "Resultado SQL")

    oView:AddUserButton("Imprimir Resultado SQL", "PMSPRINT", {|oView| Iif(IsObject('oGridSQL'), oGridSQL:Print(), 'Objeto Grid n�o Criado!')}, "Imprime/Exporta Resultado SQL")
    oView:AddUserButton("Pesquisar no Resultado SQL", "LOCALIZA", {|oView| Iif(IsObject('oGridSQL'), oGridSQL:Search(), 'Objeto Grid n�o Criado!')}, "Pesquisa no Resultado SQL")

    oView:SetAfterViewActivate({|| ModifyObj()})

    oView:SetCloseOnOk( {|| .T.} )

Return oView


/*/{Protheus.doc} IDAEXP
@description Ponto de Entrada Geral da Rotina
	
@author  Helitom Silva
@since   23/08/2016
		
@param PARAMIXB, Array, [Objeto][IdPonto][IdModel]

@return uRet, Indefinido, Retorno diverso do PE

/*/
User Function MIDAEXP()

    Local uRet		 := .T.
    Local oObj       := ParamIXB[1]
    Local cIdPonto   := ParamIXB[2]
    Local cIdModel   := ParamIXB[3]
    Local lIsGrid    := .F.
    Local nX		 := 0
    Local nLinha     := 0
    Local nQtdLinhas := 0
    Local cRecomenda := ''
    Local aAreaOld   := GetArea()

    If (lIsGrid := ( oObj:ClassName() == 'FWFORMGRID' ))

        nQtdLinhas := oObj:GetQtdLine()
        nLinha     := oObj:nLine

    EndIf

    If cIdPonto == 'MODELPRE' /* Chamada antes da altera��o de qualquer campo do modelo. */
    ElseIf cIdPonto == 'MODELPOS' /* Chamada na valida��o total do modelo. */

        uRet := .T.

    ElseIf cIdPonto == 'FORMPRE' /* Chamada na antes da altera��o de qualquer campo do formul�rio. */
    ElseIf cIdPonto == 'FORMPOS' /* Chamada na valida��o total do formul�rio. */

    ElseIf cIdPonto == 'FORMLINEPRE' /* Chamada na pre valida��o da linha do formul�rio. */
    ElseIf cIdPonto == 'FORMLINEPOS' /* Chamada na valida��o da linha do formul�rio. */

        uRet := .T.

    ElseIf cIdPonto == 'MODELVLDACTIVE' /* Chamada na valida��o da ativa��o do Modelo. */

        uRet := ValidActive(oObj)

    ElseIf cIdPonto == 'MODELCOMMITTTS' /* Chamada apos a grava��o total do modelo e dentro da transa��o. */
    ElseIf cIdPonto == 'MODELCOMMITNTTS' /* Chamada apos a grava��o total do modelo e fora da transa��o. */
    ElseIf cIdPonto == 'FORMCOMMITTTSPRE' /* Chamada antes da grava��o da tabela do formul�rio. */
    ElseIf cIdPonto == 'FORMCOMMITTTSPOS' /* Chamada apos a grava��o da tabela do formul�rio. */

        If !lIsGrid

            If !(oObj:oFormModel:nOperation = MODEL_OPERATION_DELETE)

            EndIf

        EndIf

    ElseIf cIdPonto == 'MODELCANCEL' /* Cancela */

        uRet := .T.

    ElseIf cIdPonto == 'BUTTONBAR' /* Usado para Cria��o de Botoes Estrutura: { {'Nome', 'Imagem Botao', { || bBlock } } } */
    EndIf

    RestArea(aAreaOld)

Return uRet


/*/{Protheus.doc} ValidActive
@description Valida a Ativa��o do Modelo
	
@author  Helitom Silva
@since   23/08/2016
		
@param p_oModel, Objeto, Modelo

@return lRet,  Se poder alterar Retorna .T.

/*/
Static Function ValidActive(p_oModel)

    Local lRet 	 := .T.
    Local oTBZ11 := p_oModel:GetModel('TBZ11MASTER')

Return lRet


/*/{Protheus.doc} DefButtons
@description Define Botes no Formul�rio

@author  Helitom Silva
@since   23/08/2016

@param oPanel, Objeto, Objeto Panel enviado pelo MVC

/*/
Static Function DefButtons(oPanel)

    Local lret		:= .T.
    Local CzClaro	:= RetColor(244, 244, 244)
    Local bExecSQL 	:= {|| MsgRun( "Executando SQL.", "Executando", {|| ExecSQL()})}
    Local bExpoSQL 	:= {|| MsgRun( "Exportando Dados SQL.", "Exportando", {|| ExpoSQL()})}
    Local bImpSQL 	:= {|| Iif(IsObject('oGridSQL'), oGridSQL:Print(), 'Objeto Grid n�o Criado!')}
    Local bPesqSQL 	:= {|| Iif(IsObject('oGridSQL'), oGridSQL:Search(), 'Objeto Grid n�o Criado!')}

    SetPrvt('oFtArialBd', 'oBtnExec', 'oBtnExpo')

	/* Fontes */
    oFtArialBd := TFont():New( "Arial Rounded MT Bold",0, -16,,.F.,0,,400,.F.,.F.,,,,,, )

    oBtnExec   := TButton():New( 018, 006, "Executar SQL", oPanel, @bExecSQL, 047, 012,,,,.T.,,"",,,,.F. )
    oBtnExpo   := TButton():New( 034, 006, "Exportar", oPanel, @bExpoSQL, 047, 012,,,,.T.,,"",,,,.F. )
    oBtnImp    := TButton():New( 050, 006, "Imprimir", oPanel, @bImpSQL, 047, 012,,,,.T.,,"",,,,.F. )
    oBtnImp    := TButton():New( 066, 006, "Pesquisar", oPanel, @bPesqSQL, 047, 012,,,,.T.,,"",,,,.F. )

Return lret


/*/{Protheus.doc} ExecSQL
@description Execu��o do SQL e mostrando resultado na Grid

@author  Helitom Silva
@since   23/08/2016

/*/
Static Function ExecSQL()

    Local lRet		  := .T.
    Local oModel 	  := FwModelActive()
    Local oView 	  := FwViewActive()
    Local oModelTBZ11 := oModel:GetModel("TBZ11MASTER")
    Local cTMPSQL     := oModelTBZ11:GetValue(Z11SQL)

    Private oPanGrid  := oView:GetViewObj('PANELGRID')[3]

    TcQuery cTMPSQL New Alias 'TMPSQL' New

    DefGrid(oPanGrid:oOwner/*oPanel*/, .F.)

Return lRet


/*/{Protheus.doc} ExpoSQL
@description Exporta��o dos dados resultantes do SQL

@author  Helitom Silva
@since   23/08/2016

/*/
Static Function ExpoSQL(p_lStartJob)

    Local aRet		  := {.T., ''}
    Local oModel 	  := FwModelActive()
    Local oView 	  := FwViewActive()
    Local oModelTBZ11 := oModel:GetModel("TBZ11MASTER")
    Local cTMPSQL     := oModelTBZ11:GetValue(Z11SQL)
    Local cDirExp     := oModelTBZ11:GetValue(Z11DIR)
    Local cFileExp    := oModelTBZ11:GetValue(Z11FILE)
    Local cTpTField   := oModelTBZ11:GetValue(Z11TFIELD)
    Local aStruSQL	  := {}
    Local nX		  := {}
    Local cDescField  := ''
    Local cContExp	  := ''
    Local cDelCSV	  := oModelTBZ11:GetValue(Z11DELCSV)
    Local oError
    Local cLogErro	  := ''

    Default p_lStartJob := IsBlind()

	/* Salva bloco de c�digo do tratamento de erro - Fonte: http://tdn.totvs.com/display/tec/ErrorBlock */
    oError 	 := ErrorBlock({|e| aRet := {.F., e:ErrorStack}, cLogErro := 'Erro na Execu��o do SQL Exporta��o ' + CR + e:ErrorStack, GravLog(cLogErro, 'IDAEXPAG', 'SQL_EXPORTACAO_DADOS'), MsgStop(cLogErro), DFBreak()})

    Begin Transaction

        Begin Sequence

            aAreaTBZ11	:= GetArea(TBZ11)

            Iif(Select('ALIASTMP') > 0, ('ALIASTMP')->(DbCloseArea()), Nil)

            TcQuery cTMPSQL New Alias 'ALIASTMP' New

            aStruSQL := ('ALIASTMP')->(DbStruct())

            For nX := 1 to Len(aStruSQL)

                If cTpTField = 'D'

                    cDescField := AllTrim(RetDSX3(aStruSQL[nX, 1])[18])

                    cContExp +=  Iif(!Empty(cDescField), cDescField, aStruSQL[nX, 1]) + Iif(nX != Len(aStruSQL), cDelCSV, '')

                Else

                    cContExp += aStruSQL[nX, 1] + Iif(nX != Len(aStruSQL), cDelCSV, '')

                EndIf

            Next

            cContExp += CR

            While .not. ('ALIASTMP')->(Eof())

                For nX := 1 to Len(aStruSQL)

                    If aStruSQL[nX][2] == 'C'
                        cFldValue := ('ALIASTMP')->(FieldGet(nX))
                    ElseIf aStruSQL[nX][2] == 'N'
                        cFldValue := Str(('ALIASTMP')->(FieldGet(nX)))
                    ElseIf aStruSQL[nX][2] == 'D'
                        cFldValue := CtoD(('ALIASTMP')->(FieldGet(nX)))
                    ElseIf aStruSQL[nX][2] == 'L'
                        cFldValue := ('ALIASTMP')->(FieldGet(nX))
                    EndIf

                    cContExp += cFldValue + Iif(nX != Len(aStruSQL), cDelCSV, '')

                Next

                cContExp += CR

                ('ALIASTMP')->(DbSkip())
            End

            ('ALIASTMP')->(DbCloseArea())

            MemoWrite(AllTrim(cDirExp) + AllTrim(cFileExp) + '.csv', cContExp)

            Recover

            DisarmTransaction()

        End Sequence

    End Transaction

    ErrorBlock(oError)

Return aRet


/*/{Protheus.doc} DefGrid
@description Define Grid para Amostragem dos dados retornados pelo SQL

@author  Helitom Silva
@since   23/08/2016

@param oPanel, Objeto, Objeto Panel enviado pelo MVC
@param p_lDefault, Logico, Define se ira criar somente os campos Default

/*/
Static Function DefGrid(oPanel, p_lDefault)

    Local lRet		   := .T.
    Local oModel 	   := FwModelActive()
    Local oView 	   := FwViewActive()
    Local oModelTBZ11  := oModel:GetModel("TBZ11MASTER")
    Local nX		   := 0

    Default p_lDefault := .T.

    oGridSQL := IdaGrid():Create(012,010,273,443, 0, 'AllwaysTrue()', 'AllwaysTrue()', '',, 0, 99, 'AllwaysTrue()', '', 'AllwaysTrue()', oPanel, HeaderSQL(p_lDefault), {}, 0, 0)
    oGridSQL:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
    oGridSQL:SetRel('IDAEXP', 'Relatorio de SQL Exporta��o: ' + oModelTBZ11:GetValue(Z11DESC), 'SQL Exporta��o', .T.)

    If .not. p_lDefault

        oGridSQL:Limpar()

        While .not. TMPSQL->(Eof())

            oGridSQL:AddLinha()

            For nX := 1 to oGridSQL:GetQtdColuna()
                oGridSQL:SetColuna(oGridSQL:NomeColuna(nX), TMPSQL->&(oGridSQL:NomeColuna(nX)))
            Next

            TMPSQL->(DbSkip())
        End

        oGridSQL:PosLinha(1)

        TMPSQL->(DbCloseArea())

    EndIf

Return lRet


/*/{Protheus.doc} HeaderSQL
@description Monta aHeader da Grid de SQL

@author  Helitom Silva
@since   23/08/2016

@param p_lDefault, Logico, Define se ira criar somente os campos Default

/*/
Static Function HeaderSQL(p_lDefault)

    Local aRet     := {}
    Local nX 	   := 1
    Local aStruSQL := {}

    Default p_lDefault := .T.

    If p_lDefault
        aAdd(aRet, {"SQL", "SQL", "@!", 10, 0, "AllWaysTrue()", "�", "C", "", "R",,,,"V"})
    Else

        aStruSQL := TMPSQL->(DbStruct())

        For nX := 1 to Len(aStruSQL)

            aAdd(aRet, {aStruSQL[nX, 1], aStruSQL[nX, 1], "@!", aStruSQL[nX, 3], aStruSQL[nX, 4], "AllWaysTrue()", "�", aStruSQL[nX, 2], "", "R",,,,"V"})

        Next

    EndIf

Return aRet


/*/{Protheus.doc} ModifyObj
@description Modifica propriedades dos Objetos.

@author  Helitom Silva
@since   26/08/2016
@version 1.0

/*/
Static Function ModifyObj()

    Local oTmpDlg 	  := Nil
    Local nX, nI  	  := 0
    Local cListObj    := ''
    Local oView		  := FwViewActive()
    Local oFtArialB12 := TFont():New( "Arial",0,-12,,.T.,0,,400,.F.,.F.,,,,,, )

    If oView:IsActive() .and. ValType(oView:GetModel('TBZ11MASTER')) = 'O'

        oTmpDlg := GetWndDefault()

        For nX := 1 to Len(oTmpDlg:aControls)

            If ValType(oTmpDlg:aControls[nX]) == 'O'
                cListObj += cValToChar(nX) + ' - ' + AllTrim(oTmpDlg:aControls[nX]:ClassName()) + CR
            EndIf

            If ValType(oTmpDlg:aControls[nX]) == 'O' .and. 'FWMULTIGET' $ AllTrim(oTmpDlg:aControls[nX]:ClassName())

                oTmpDlg:aControls[nX]:Align := CONTROL_ALIGN_ALLCLIENT
                oTmpDlg:aControls[nX]:SetFont(oFtArialB12)
                oTmpDlg:aControls[nX]:EnableVScroll( .F. )
                oTmpDlg:aControls[nX]:Refresh()

            EndIf

        Next

        oTmpDlg:Refresh()

    EndIf

    //MemoWrite('C:\Temp\Obj-Classes.txt', cListObj)

Return


/*/{Protheus.doc} IDAEXPAG
@description Processo de Exporta��o do Resultado dos SQL Agendados

@author  Helitom Silva
@since   23/08/2016

@param p_lStartJob, Logico, Define se esta sendo executado como Job .T. caso contrario .F..
@param p_aEmpFil, Array, Array de com dados da empresa.

/*/
User Function IDAEXPAG(p_lStartJob, p_aEmpFil)

    Local aRet		  := {.T., ''}
    Local aAreaTBZ11  := {}
    Local cTMPSQL     := ''
    Local cDirExp     := ''
    Local cFileExp    := ''
    Local cTpTField   := ''
    Local cDescField  := ''
    Local aStruSQL	  := {}
    Local nX		  := {}
    Local cContExp	  := ''
    Local nNumLSQL	  := 0
    Local cDelCSV	  := ''
    Local oError
    Local cLogErro	  := ''

    Default p_lStartJob := .F.

	/* Salva bloco de c�digo do tratamento de erro - Fonte: http://tdn.totvs.com/display/tec/ErrorBlock */
    oError := ErrorBlock({|e| aRet := {.F., e:ErrorStack}, cLogErro := 'Erro na Execu��o do SQL Exporta��o ' + CR + e:ErrorStack, GravLog(cLogErro, 'IDAEXPAG', 'SQL_EXPORTACAO_DADOS'), Iif(!p_lStartJob, MsgStop(cLogErro), ConOut(cLogErro)), DFBreak()})

    Begin Transaction

        Begin Sequence

            If p_lStartJob
                RpcSetType( 3 )
                RpcSetEnv(p_aEmpFil[1], p_aEmpFil[2]/*, 'Usuario', 'Senha'*/)
            EndIf

            aAreaTBZ11	:= GetArea(TBZ11)

            DbSelectArea(TBZ11)
            (TBZ11)->(DbSetOrder(1))

            (TBZ11)->(DbGoTop())
            While .not. (TBZ11)->(EoF())

                cTMPSQL	 := ''
                nNumLSQL := MlCount(AllTrim(GetValue(TBZ11, Z11SQL)), 40)

                //nHandle := FCreate(AllTrim(cDirExp) + AllTrim(cFileExp) + '.csv')

                If nNumLSQL = 0
                    (TBZ11)->(DbSkip())
                    Loop
                ElseIf AllTrim(GetValue(TBZ11, Z11MSBLQL)) = '1'
                    (TBZ11)->(DbSkip())
                    Loop
				/*ElseIf Empty(GetValue(TBZ11, Z11DTEXP)) .or. (dDataBase > GetValue(TBZ11, Z11DTEXP))
                    If .not. Empty(GetValue(TBZ11, Z11HREXP))
                        If .not. (Time() > GetValue(TBZ11, Z11HREXP))
							(TBZ11)->(DbSkip())
							Loop				
                        EndIf
                    Else
						(TBZ11)->(DbSkip())
						Loop			
                    EndIf*/
                EndIf
			     
                For nX := 1 to nNumLSQL
			        cTMPSQL += MemoLine(GetValue(TBZ11, Z11SQL),, nX)        
                Next
			    
			    cDelCSV	  := GetValue(TBZ11, Z11DELCSV)
				cDirExp   := GetValue(TBZ11, Z11DIR)
				cFileExp  := GetValue(TBZ11, Z11FILE)		
				cTpTField := GetValue(TBZ11, Z11TFIELD)		
				
				TcQuery cTMPSQL New Alias 'TMPSQL' New
					
				aStruSQL := TMPSQL->(DbStruct())
			
                For nX := 1 to Len(aStruSQL)
					
                    If cTpTField = 'D'
						
						cDescField :=  AllTrim(RetDSX3(aStruSQL[nX, 1])[18])
						
						cContExp +=  Iif(!Empty(cDescField), cDescField, aStruSQL[nX, 1]) + Iif(nX != Len(aStruSQL), cDelCSV, '')
						
                    Else
						
						cContExp += aStruSQL[nX, 1] + Iif(nX != Len(aStruSQL), cDelCSV, '')
					
                    EndIf
					
                Next
				
				cContExp += CR
				
                While .not. TMPSQL->(Eof())
					
                    For nX := 1 to Len(aStruSQL)
						
                        If aStruSQL[nX][2] == 'C'
							cFldValue := TMPSQL->(FieldGet(nX))
                        ElseIf aStruSQL[nX][2] == 'N'
							cFldValue := Str(TMPSQL->(FieldGet(nX)))
                        ElseIf aStruSQL[nX][2] == 'D'
							cFldValue := CtoD(TMPSQL->(FieldGet(nX)))
                        ElseIf aStruSQL[nX][2] == 'L'
							cFldValue := TMPSQL->(FieldGet(nX))
                        EndIf
						
						cContExp += cFldValue + Iif(nX != Len(aStruSQL), cDelCSV, '')
						
                    Next

					cContExp += CR					
					//FWrite(nHandle, cContExp)
							
					TMPSQL->(DbSkip())
                End
				
				Iif(Select('TMPSQL') > 0, TMPSQL->(DbCloseArea()), Nil)
					
				MemoWrite(AllTrim(cDirExp) + AllTrim(cFileExp) + '.csv', cContExp)
				//FClose(nHandle)
					
				cContExp := ''
				
				(TBZ11)->(DbSkip())
            End
		
			RestArea(aAreaTBZ11)

            If p_lStartJob
				RpcClearEnv()
            EndIf
				
		Recover
					
			DisarmTransaction()
			
        End Sequence

    End Transaction
		
	ErrorBlock(oError)
			
Return aRet