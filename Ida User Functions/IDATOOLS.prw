#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE "SMXFUN.CH"
#INCLUDE "SDIC.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "ISAMQRY.CH"
#INCLUDE "IDATOOLS.CH"


/*/{Protheus.doc} IdaTools
@description Neste arquivo sao armazenadas User Functions genericas

@author Helitom Silva
@since  05/06/2012

/*/


/*/{Protheus.doc} IIsNumber
@description Avalia��o se uma string tenha apenas numeros

@author Helitom Silva
@since  05/06/2012

@param p_cString, Caracter, String a ser avaliada.

@return lRet, Logico, Se a string contem apenas numeros retorna .T.

/*/
User Function IIsNumber(p_cString, p_lAlert)
Return SoNumeros(p_cString, p_lAlert)


/*/{Protheus.doc} ISelFile
@description Retorna um caminho do arquivo selecionado ou para ser salvo.

@author Helitom Silva
@since  05/06/2012

@param p_cTitulo, Caracter, Titulo da janela.
@param p_cMasc, Caracter, Mascara para aparecer apenas arquivo com extencao especifica. Exemplo: Arquivos csv (*.csv) |*.csv| ou "Arquivos Texto (*.TXT) |*.txt|
@param p_lSalva, Logico, Se .T. mostra botao de salvar senao mostra botao de abrir para selecionar o arquivo.

@return cRet, Caracter, Caminho do arquivo selecionado ou salvo

/*/
User Function ISelFile(p_cTitulo, p_cMasc, p_lSalva)
Return SeleArq(p_cTitulo, p_cMasc, p_lSalva)


/*/{Protheus.doc} IDim
@description Funcao responsavel por manter o Layout independente da resolucao horizontal do Monitor do Usuario.

@author Norbert/Ernani/Mansano
@since  10/05/2005

@param p_nTam, Numerico, Tamanho/Posi��o a ser refeita.

/*/
User Function IDim(p_nTam)
Return Dim(p_nTam)


/*/{Protheus.doc} ADVtoSQL
@description Retorna um filtro SQL, baseado em instru��o de filtro ADVPL

@author Helitom Silva
@since  21/08/2012

@param p_cFilADV, Caracter, String com Filtro de sintaxe ADVPL

@return cRet, Caracter, String com filtro de Sintaxe SQL

/*/
User Function ADVtoSQL(p_cFilADV)
Return ADVParSQL(p_cFilADV)


/*/{Protheus.doc} IDataExt
@description Retorna Data por extenso

@author Helitom Silva
@since  21/07/2013

@param p_dData, Data, Informa��o no formato data

@return cDataExt, Caracter, Data por extenso

/*/
User Function IDataExt(p_dData)
Return DataExt(p_dData)


/*/{Protheus.doc} IRetColor
@description Retorna Codido de Cor RGB

@author Helitom Silva
@since  03/08/2013

@param p_nRed, Numerico, Quantidade de Vermelho (0..255)
@param p_nGreen, Numerico, Quantidade de Verde (0..255)
@param p_nBlue, Numerico, Quantidade de Azul (0..255)

@return nRet, Numerico, codigo da cor

/*/
User Function IRetColor(p_nRed, p_nGreen, p_nBlue)
Return RetColor(p_nRed, p_nGreen, p_nBlue)


/*/{Protheus.doc} IArVlCima
@description Arrendondar valor para acima

@author Helitom Silva
@since  03/08/2013

@param p_nValor, Numerico, Valor
@param p_nCasDec, Numerico, Casas decimais

@return nRet, Numerico, Valor arredondado para cima, conforme casas decimais

/*/
User Function IArVlCima(p_nValor, p_nCasDec)
Return ArVlCima(p_nValor, p_nCasDec)


/*/{Protheus.doc} IConfirm
@description Mensagem de Confirmacao

@author	Helitom Silva
@since  22/08/2013

@param p_p_cMsg, Caracter, Mensagem
@param p_cTitulo, Caracter, Titulo da Tela
@param p_aOpc, Array, Array com dois itens, para identificar os nomes dos botoes exemplo: {'Sim', 'N�o'}
@param p_nFocus, Numerico, Qual opcao ter� o foco
	 
@return Se .T. se confirmou e .F. se nao confirmou

/*/
User Function IConfirm(p_cMsg, p_cTitulo, p_aOpc, p_nFocus)
Return Confirm(p_cMsg, p_cTitulo, p_aOpc, p_nFocus)


/*/{Protheus.doc} ITimeExec
@description Demonstra��o de Tempo de Execu��o - Esta funcao poder� ser usada para medir o tempo de execu��o de determinada Rotina, Consulta SQL, calculo e etc..

@author Helitom Silva
@since  27/08/2013

@param p_nSegIni, Numerico, Informe o tempo inicial por meio da fun��o "Seconds()"
@param p_nSegFim, Numerico, Informe o tempo final por meio da fun��o "Seconds()"

@return cRet, Caracter, String com Hora:Minuto:Segundos

/*/
User Function ITimeExec(p_nSegIni, p_nSegFim)
Return TimeExec(p_nSegIni, p_nSegFim)


/*/{Protheus.doc} IIsProcCall
@description Retorna se uma determinada funcao esta na pilha de Execu��o.

@author Helitom Silva
@since  02/09/2013

@param p_cRotina, Caracter, Informe a funcao que deseja verificar

@return lRet, Logico, Retorna se .T. se a funcao esta na pilha, senao .F.. 

/*/
User Function IIsProcCall(p_cRotina)
Return IsProcCall(p_cRotina)


/*/{Protheus.doc} IOpeURLP
@description Abre um URL numa tela de browser de Internet dentro do Protheus

@author Helitom Silva
@since  16/09/2013

@param p_cURL, Caracter, URL

@link http://tdn.totvs.com/display/tec/TIBrowser
/*/
User Function IOpeURLP(p_cURL)
Return OpenURLP(p_cURL)


/*/{Protheus.doc} IOpeURLB
@description Abre um URL no browser de Internet padrao do Windows.

@author Helitom Silva
@since  16/09/2013

@param p_cURL, Caracter, URL

@link http://tdn.totvs.com/display/tec/TIBrowser
/*/
User Function IOpeURLB(p_cURL)
Return OpenURLB(p_cURL)


/*/{Protheus.doc} IPutMv
@description Verifica se existe um parametro e cria se necess�rio

@author Helitom Silva
@since  28/03/2012

@param p_cMvPar, Caracter, Parametro
@param p_cValor, Caracter, Valor do Parametro
@param p_cFilial, Caracter, Filial
@param p_cDesc, Caracter, Descri��o do paramento

@return lRet, Logico, .T.

/*/
User Function IPutMv(p_cMvPar, p_cValor, p_cFilial, p_cDesc)
Return IPutMv(p_cMvPar, p_cValor, p_cFilial, p_cDesc)


/*/{Protheus.doc} IGetMV
@description Pesquisa um parametro, sempre buscando na tabela sx6

@author Helitom Silva
@since  28/03/2012

@param p_cMvPar, Caracter, Parametro
@param p_cDef, Caracter, Valor Default
@param p_cFilial, Caracter, Filial

@return Conte�do do Parametro ou Valor Default

/*/
User Function IGetMV(p_cMvPar, p_cDef, p_cFilial)
Return IdaGetMV(p_cMvPar, p_cDef, p_cFilial)


/*/{Protheus.doc} IRetABox
@description converte CBox (SX3) em Array ponto para o objeto da classe TComBox.

@author Helitom Silva
@since  29/12/2013

@param cBox, Caracter, Dados X3_CBOX (Exemplo: S=Sim;N=N�o)

@return aBox, Array, Lista com opcoes do Combobox, ponto para o objeto da classe TComBox

/*/
User Function IRetABox(p_cBox)
Return RetABox(p_cBox)


/*/{Protheus.doc} INameEnt
@description Retorna o nome da Entidade do Arquivo XX8

@author Julio Storino
@since  30/11/2012

@param p_nEnt, Numerico, Informe o tipo de retorno ( 0 - Nome do Grupo, 1 - Nome da Unidade de Negocio, 2 - Nome da Empresa, 3 - Nome da Filial ) 

@return cRet, Caracter, Nome desejado conforme parametro p_nEnt

@obs Valores Default p_cEmp=01 p_cFil=01001001 p_cEnt=3

/*/
User Function INameEnt(p_cEmp, p_cFil, p_nEnt)
Return RNomeEnt(p_cEmp, p_cFil, p_nEnt)


/*/{Protheus.doc} IGravLog
@description Grava Logs

@author  Julio Storino
@since   03/11/2007
@version 1.0

@param p_cMsg, Caracter, Mensagem do Log
@param p_cTipo, Caracter, Tipo do Log
@param p_cName, Caracter, Nome do Log

@author  Helitom Silva
@since   20/01/2015
@version 2.0 - Feito tratamento para gravar no system definido no ambiente em execu��o

/*/
User Function IGravLog(p_cMsg, p_cTipo, p_cName, p_lHelp)
Return GravLog(p_cMsg, p_cTipo, p_cName, p_lHelp)


/*/{Protheus.doc} ISavePar
@description Atualiza SX1 Antes de Carregar valores Salvos

@author Julio Storino
@since  06/12/2011

@param p_cPergunta, Caracter, Nome do Grupo de Perguntas
@param p_aOrdVal, Caracter, Nome do Grupo de Perguntas

/*/
User Function ISavePar(p_cPergunta, p_aOrdVal)
Return SavePar(p_cPergunta, p_aOrdVal)


/*/{Protheus.doc} IPCPFCNPJ
@description Retorna o picture de formata��o para CPF ou CNPJ
	
@author  Helitom Silva
@since   21/08/2014
@version 1.0
		
@param p_cCPFCNPJ, Caracter, Numero do CPF ou CNPJ

@return cRet, Caracter, Picture do documento informado

/*/
User Function IPCPFCNPJ(p_cCPFCNPJ)
Return PCPFCNPJ(p_cCPFCNPJ)


/*/{Protheus.doc} ITCPFCNPJ
@description Formata��o para CPF ou CNPJ
	
@author  Helitom Silva
@since   21/08/2014
@version 1.0
		
@param p_cCPFCNPJ, Caracter, Numero do CPF ou CNPJ

@return cRet, Caracter, CPF/CNPJ formatado

/*/
User Function ITCPFCNPJ(p_cCPFCNPJ)
Return TCPFCNPJ(p_cCPFCNPJ)


/*/{Protheus.doc} IRetDado
@description Retorna dado do tipo informado no parametro
	
@author  Helitom Silva
@since   28/08/2014
@version 1.0		

@return p_cTipo, Caracter, Sigla do Tipo de dados (C = caracter, N = Numerico, D = Data, L = Logico, U = Indefinido)

/*/
User Function IRetDado(p_cTipo)
Return RetDado(p_cTipo)


/*/{Protheus.doc} IExistTable
@description Retorna se existe uma tabela no dicionario de dados.
	
@author  Helitom Silva
@since   08/10/2014
@version 1.0		

@return p_cAlias, Caracter, Alias da Tabela a ser verificada

@return lRet, Logico, Se a tabela existir retorna .T.

/*/
User Function IExistTable(p_cAlias)
Return ExistTable(p_cAlias)


/*/{Protheus.doc} IEstAll
@description Calcula o saldo de estoque de determinado produto em uma data.

@author Julio Kusther
@since 27/10/2014
@version 1.0

@param p_CodPro, Caracter, C�digo do produto a ser consultado
@param p_dData, Data, Data do saldo a ser consultado

@return nSaldo, Saldo do produto em todos os locais de estoque
/*/
User Function IEstAll(p_CodPro, p_dData)
Return EstAll(p_CodPro, p_dData)


/*/{Protheus.doc} IFJ
@description Retorna mascara de picture campo.

@author Geanderson Silva
@since 15/11/2014
@version 1.0

@param p_cCampo,caracter,Conteudo do campo que ser� formatado.
@param p_cTipo,caracter, Tipo se pessoa fisica ou Juridica.

@return cMasc, caracter, Marcara do campo CPF ou CNPJ.

/*/
User Function IFJ(p_cCampo,p_cTipo)
Return XFJ(p_cCampo,p_cTipo)


/*/{Protheus.doc} IBoxSx1
@description Fun��o semelhante a paramBox

@author Julio Kusther
@since 16/12/2014
@version 1.0

/*/
User Function IBoxSx1(aParametros,cTitle,aRet,bOk,aButtons,lCentered,nPosx,nPosy)
Return BoxSx1(aParametros,cTitle,aRet,bOk,aButtons,lCentered,nPosx,nPosy)


/*/{Protheus.doc} IMsgInfo
@description Fun��o semelhante a paramBox

@author  Helitom Silva
@since   18/11/2016
@version 1.0

@param p_cMsg, Caracter, Mensagem
@param p_cTitulo, Caracter, Titulo da Janela

/*/
User Function IMsgInfo(p_cMsg, p_cTitulo)
Return IdaMsg(p_cMsg, p_cTitulo)


/*/{Protheus.doc} iPutSX1
@description Funcao Substituta do PutSX1

@author  Helitom Silva
@since   07/08/2017
@version 1.0 - Copiado do HelpFacil

@param cGrupo, characters, descricao
@param cOrdem, characters, descricao
@param cPergunt, characters, descricao
@param cPerSpa, characters, descricao
@param cPerEng, characters, descricao
@param cVar, characters, descricao
@param cTipo, characters, descricao
@param nTamanho, numeric, descricao
@param nDecimal, numeric, descricao
@param nPresel, numeric, descricao
@param cGSC, characters, descricao
@param cValid, characters, descricao
@param cF3, characters, descricao
@param cGrpSxg, characters, descricao
@param cPyme, characters, descricao
@param cVar01, characters, descricao
@param cDef01, characters, descricao
@param cDefSpa1, characters, descricao
@param cDefEng1, characters, descricao
@param cCnt01, characters, descricao
@param cDef02, characters, descricao
@param cDefSpa2, characters, descricao
@param cDefEng2, characters, descricao
@param cDef03, characters, descricao
@param cDefSpa3, characters, descricao
@param cDefEng3, characters, descricao
@param cDef04, characters, descricao
@param cDefSpa4, characters, descricao
@param cDefEng4, characters, descricao
@param cDef05, characters, descricao
@param cDefSpa5, characters, descricao
@param cDefEng5, characters, descricao
@param aHelpPor, array, descricao
@param aHelpEng, array, descricao
@param aHelpSpa, array, descricao
@param cHelp, characters, descricao

@return return, return_description

/*/
User Function iPutSX1(cGrupo, cOrdem, cPergunt, cPerSpa, cPerEng, cVar,;
        cTipo, nTamanho, nDecimal, nPresel, cGSC, cValid,;
        cF3, cGrpSxg, cPyme,;
        cVar01, cDef01, cDefSpa1,cDefEng1,cCnt01,;
        cDef02, cDefSpa2, cDefEng2,;
        cDef03, cDefSpa3, cDefEng3,;
        cDef04, cDefSpa4, cDefEng4,;
        cDef05, cDefSpa5, cDefEng5,;
        aHelpPor, aHelpEng, aHelpSpa, cHelp)

    iPutSX1(cGrupo, cOrdem, cPergunt, cPerSpa, cPerEng, cVar,;
        cTipo, nTamanho, nDecimal, nPresel, cGSC, cValid,;
        cF3, cGrpSxg, cPyme,;
        cVar01, cDef01, cDefSpa1,cDefEng1,cCnt01,;
        cDef02, cDefSpa2, cDefEng2,;
        cDef03, cDefSpa3, cDefEng3,;
        cDef04, cDefSpa4, cDefEng4,;
        cDef05, cDefSpa5, cDefEng5,;
        aHelpPor, aHelpEng, aHelpSpa, cHelp)

Return


/*/{Protheus.doc} iPutSX1Help
@description Funcao Substituta do PutSX1Help

@author  Helitom Silva
@since   07/08/2017
@version 1.0

@param cKey, characters, descricao
@param aHelpPor, array, descricao
@param aHelpEng, array, descricao
@param aHelpSpa, array, descricao
@param lUpd, logical, descricao
@param cStatus, characters, descricao

@return return, return_description

/*/
User Function iPutSX1Help(cKey, aHelpPor, aHelpEng, aHelpSpa, lUpd, cStatus)

    iPutSX1Help(cKey, aHelpPor, aHelpEng, aHelpSpa, lUpd, cStatus)

Return


/*/{Protheus.doc} iRChrRight
@description Remove caracter a direita da String.

@author  Helitom Silva
@since   21/10/2019
@version 1.0

@return cRet, Caracter, return_description

@param p_cString, Caracter, Texto que ter� os caracteres removidos
@param p_cChar, Caracter, Caracter a ser removido

@type function
@see  (links_or_references)
/*/
User Function iRChrRight(p_cString, p_cChar)

    Local cRet := ''
    Local nX   := 0

    Default p_cString := ''
    Default p_cChar   := ''

    cRet := AllTrim(p_cString)

    For nX := Len(p_cString) to 1 Step -1

        If SubStr(cRet, nX, 1) = p_cChar
            cRet := Left(cRet, nX - 1)
        Else
            Exit
        EndIf

    Next

Return cRet