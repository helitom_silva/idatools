#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TOTVS.CH'


/*/{Protheus.doc} HSDialog
@description Classe de Dialogo Helitom.
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Class HSDialog From MSDialog

    Data cOrigem HIDDEN

    Method New() CONSTRUCTOR
    Method Activate()
    Method SetOrigem()
    Method GetOrigem()
    Method SearchOrigem()
    Method SetKey()

EndClass


/*/{Protheus.doc} New
@description Inicia Objeto HSDialog - (Construtor)
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method New(  nTop,  nLeft,  nBottom,  nRight,  cCaption,  uParam6,  uParam7,  uParam8,  uParam9,  nClrText,  nClrBack,  uParam12,  oWnd,  lPixel,  uParam15,  uParam16,  uParam17,  lTransparent ) CLASS HSDialog
    :New(  nTop,  nLeft,  nBottom,  nRight,  cCaption,  uParam6,  uParam7,  uParam8,  uParam9,  nClrText,  nClrBack,  uParam12,  oWnd,  lPixel,  uParam15,  uParam16,  uParam17,  lTransparent )

    Self:SearchOrigem()

Return Self


/*/{Protheus.doc} Activate
@description Ativa e exibe Formul�rio
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method Activate(uParam1,  uParam2,  uParam3,  lCentered,  bValid,  uParam6,  bInit,  uParam8,  uParam9) Class HSDialog

    Self:SetKey(,,3) /* Inibe teclas de atalhos j� definidas */

    SetKey(K_CTRL_0, {|| oForm := GETWNDDEFAULT(), Iif(Upper(oForm:ClassName()) == Upper('HSDialog'), MsgInfo('Formul�rio criado na Fun��o: ' + oForm:cOrigem , 'Dados do Formul�rio'), MsgInfo('N�o encontrado fun��o de Origem do Formul�rio.'))})

    _Super:Activate(  uParam1,  uParam2,  uParam3,  lCentered,  {|| Iif(ValType(bValid) == 'B', Iif( Eval(bValid), Self:SetKey(,,2), .F.), Self:SetKey(,,2))},  uParam6,  bInit,  uParam8,  uParam9 )

Return


/*/{Protheus.doc} SetOrigem
@description Define Origem
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method SetOrigem(p_cOrigem) Class HSDialog

    Self:cOrigem := p_cOrigem

Return


/*/{Protheus.doc} GetOrigem
@description Obtem Origem
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method GetOrigem() Class HSDialog
Return Self:cOrigem


/*/{Protheus.doc} SearchOrigem
@description Pesquisa Origem
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method SearchOrigem() Class HSDialog

    Local nX     := 1
    Local cVazio := AllTrim(ProcName(nX))

    While !Empty(cVazio)
        If SubStr(Upper(AllTrim(ProcName(nX))), 1, 2) = 'U_'
            Self:SetOrigem(AllTrim(ProcName(nX)))
            Exit
        Else
            nX++
            cVazio := AllTrim(ProcName(nX))
        EndIf
    End

Return


/*/{Protheus.doc} SetKey
@description Define, guarda e restaura atalhos
	
@author  Helitom Silva
@since   11/05/2014
@version 1.0		

/*/
Method SetKey(p_uTecla, p_bBloco, p_nTipo) Class HSDialog

    Local cProcAnt   := ''
    Local nX 		 := 0

    Default p_nTipo  := 1

	/*--------------------------|p_nTipo|--------------------------
		1 - Guarda/Seta os atalhos usados na Rotina
		2 - Restaura os atalhos Guardados
		3 - Inibe teclas de atalhos j� definidas
		4 - Limpa historico de defini��o Teclas de atalhos
	*/

    If Type('aHSSetKey') == 'U'
        Public aHSSetKey  := {}
    EndIf

    Do Case

    Case p_nTipo = 1

        SetKey(p_uTecla, p_bBloco)
        aAdd(aHSSetKey, {p_uTecla, p_bBloco, Self:cOrigem})

    Case p_nTipo = 2

        If Len(aHSSetKey) > 0

            For nX := Len(aHSSetKey) to 1 step - 1

                If aHSSetKey[nX][3] == Self:cOrigem

                    SetKey(aHSSetKey[nX][1], Nil)

                    aDel(aHSSetKey, nX)
                    aSize(aHSSetKey, nX - 1)

                    Loop

                EndIf

                If Empty(AllTrim(cProcAnt)) .and. !(Self:cOrigem == aHSSetKey[nX][3])
                    cProcAnt := aHSSetKey[nX][3]
                EndIf

                If (aHSSetKey[nX][3] == cProcAnt)

                    SetKey(aHSSetKey[nX][1], Nil)

                    If ValType(aHSSetKey[nX][2]) == 'B'
                        SetKey(aHSSetKey[nX][1], aHSSetKey[nX][2])
                    EndIf

                EndIf

            Next

        EndIf

    Case p_nTipo = 3

        If Len(aHSSetKey) > 0

            For nX := Len(aHSSetKey) to 1 step - 1

                If Empty(AllTrim(cProcAnt)) .and. !(Self:cOrigem == aHSSetKey[nX][3])
                    cProcAnt := aHSSetKey[nX][3]
                EndIf

                If (aHSSetKey[nX][3] == cProcAnt)

                    SetKey(aHSSetKey[nX][1], Nil)

                EndIf

            Next

        EndIf

			/* Defini��o da tecla de atalho F24 com bloco Nil, para que na restaura��o volte apenas um Nivel de Formul�rio */
        Self:SetKey(135, Nil)

    Case p_nTipo = 4

        aHSSetKey := Nil

    EndCase

Return