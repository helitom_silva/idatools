#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "IDATOOLS.CH"
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'


/*/{Protheus.doc} IdaTBC
@description Cadastro de Tabelas de Customiza��o.
	
@author  Helitom Silva
@since   29/02/2016
@version 1.0	

/*/
User Function IDATBC()

    Local oTabCust	   := IdaTBC():New()

    Private cAliasTBC  := oTabCust:GetTable()
    Private cModel	   := cAliasTBC + '_MODEL'
    Private cFunction  := 'IdaTBC'
    Private cTela	   := 'Esta��o'
    Private oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias( cAliasTBC )
    oBrowse:SetDescription( 'Cadastro de Tabelas de Customiza��o' )
    //oBrowse:SetFilterDefault("" + cAliasTBC + "->" + cAliasTBC + "_FILIAL == xFilial('" + cAliasTBC + "') .AND. " + cAliasTBC + "->" + cAliasTBC + "_CODIGO = cIpEst")

    oBrowse:Activate()

Return


/*/{Protheus.doc} MenuDef
@description Retorna os itens para constru��o do menu da rotina
	
@author  Helitom Silva
@since   29/02/2016
@version 1.0

@return aRotina, Array, Lista com os itens do menu

/*/
Static Function MenuDef()

    Local aRotina := {}

    aAdd( aRotina, { "Visualizar", "ViewDef.IdaTBC", 0, 2, 0, Nil } )
    aAdd( aRotina, { "Incluir"   , "ViewDef.IdaTBC", 0, 3, 0, Nil } )
    aAdd( aRotina, { "Alterar"   , "ViewDef.IdaTBC", 0, 4, 0, Nil } )
    aAdd( aRotina, { "Excluir"   , "ViewDef.IdaTBC", 0, 5, 0, Nil } )

Return aRotina


/*/{Protheus.doc} ModelDef
@description Retorna o modelo padrao para a rotina
	
@author  Helitom Silva
@since   29/02/2016
@version 1.0
@return oModel, Objeto, Modelo de dados

/*/
Static Function ModelDef()

    Local oStruTBC := FWFormStruct( 1, cAliasTBC)
    Local oModel

    oModel := MPFormModel():New( 'UIDATBC')
    oModel:AddFields( cModel, , oStruTBC )

Return oModel


/*/{Protheus.doc} ViewDef
@description Retorna os View do modelo de dados
	
@author  Helitom Silva
@since   29/02/2016
@version 1.0

@return oView, Logico, View do modelo de dados

/*/
Static Function ViewDef()

    Local oModel	:= FWLoadModel( "IDATBC" )
    Local oStruTBC	:= FWFormStruct( 2, cAliasTBC )
    Local oView

    oView := FWFormView():New()

    oView:SetModel( oModel )
    oView:AddField( cModel, oStruTBC)
    oView:CreateHorizontalBox( "CENTER" , 50 )
    oView:SetOwnerView( cModel, "CENTER" )

Return oView


/*/{Protheus.doc} UIdaTBC
@description Ponto de Entrada Geral da Rotina
	
@author  Helitom Silva
@since   29/02/2016
@version 1.0
		
@param PARAMIXB, Array, [Objeto][IdPonto][IdModel]

@return uRet,  Retorno diversos do PE

/*/
User Function UIDATBC()

    Local uRet		 := .T.
    Local oObj       := ParamIXB[1]
    Local cIdPonto   := ParamIXB[2]
    Local cIdModel   := ParamIXB[3]

    Local lIsGrid    := .F.
    Local nX		 := 0
    Local nLinha     := 0
    Local nQtdLinhas := 0
    Local aAceite	 := {}
    Local aDadosChq	 := {}
    Local cBanco     := ''
    Local cAgenc   	 := ''
    Local cConta     := ''
    Local cNumChq    := ''

    If (lIsGrid := ( oObj:ClassName() == 'FWFORMGRID' ))

        nQtdLinhas := oObj:GetQtdLine()
        nLinha     := oObj:nLine

    EndIf

    If cIdPonto == 'MODELPOS' /* Chamada na valida��o total do modelo. */
    ElseIf cIdPonto == 'FORMPOS' /* Chamada na valida��o total do formul�rio. */
    ElseIf cIdPonto == 'FORMLINEPRE' /* Chamada na pre valida��o da linha do formul�rio. */
    ElseIf cIdPonto == 'FORMLINEPOS' /* Chamada na valida��o da linha do formul�rio. */
    ElseIf cIdPonto == 'MODELVLDACTIVE' /* Chamada na valida��o da ativa��o do Modelo. */
    ElseIf cIdPonto == 'MODELCOMMITTTS' /* Chamada apos a grava��o total do modelo e dentro da transa��o. */
    ElseIf cIdPonto == 'MODELCOMMITNTTS' /* Chamada apos a grava��o total do modelo e fora da transa��o. */
    ElseIf cIdPonto == 'FORMCOMMITTTSPOS' /* Chamada apos a grava��o da tabela do formul�rio. */
    ElseIf cIdPonto == 'MODELCANCEL' /* Cancela */
        uRet := .T.
    ElseIf cIdPonto == 'BUTTONBAR' /* Usado para Cria��o de Botoes Estrutura: { {'Nome', 'Imagem Botap', { || bBlock } } } */
    EndIf

Return uRet


/*/{Protheus.doc} IdaTBC
@description Consultar impressora do cadastro de esta��o.	

@author  Helitom Silva
@since   29/02/2016
@version 1.0

/*/
    Class IdaTBC

        Data cTable		As String
        Data cAliasTBC  As String

        Method New() CONSTRUCTOR
        Method GetTable()		/* Consultar Tabela usada pelo Cadastro de Tabelas de Customiza��o */
        Method InfTable()   	/* Formul�rio para informar a tabela a ser usada pelo Cadastro de Tabelas de Customiza��o  */
        Method GetTbCAlias()	/* Obtem Alias de Tabelas */
        Method GetTbCField()	/* Obtem Nome de Campos de Tabelas */
        Method PutTbCAlias()	/* Insere Novo Vinculo entre tabela customizada e Real */

    EndClass


/*/{Protheus.doc} New
@description Metodo Construtor da Classe

@author  Helitom Silva
@since   29/02/2016
@version 1.0

/*/
Method New() Class IdaTBC

    ::cTable 	:= ''
    ::cAliasTBC := ''

Return Self


/*/{Protheus.doc} GetTable
@description Retorna Alias da Tabela usada pelo Cadastro de Esta��o

@author  Helitom Silva
@since   29/02/2016
@version 1.0

/*/
Method GetTable() Class IdaTBC

    If Empty( AllTrim(Self:cTable) )

        Self:cTable	:= GetNewPar('IDA_CADTBC', '')

        While Empty( AllTrim(Self:cTable) )

            Self:cTable	:= Self:InfTable()

            If Empty( AllTrim(Self:cTable) )
                MsgAlert('N�o foi informado tabela Cadastro de Tabelas de Customiza��o, por favor, verifique seu dicion�rio de dados e informe um nome Vago!')
            Else
                IdaPutMv('IDA_CADTBC', Self:cTable, '', 'Tabela de Cadastro de Tabelas de Customiza��o')
            EndIf

        End

    EndIf

Return Self:cTable


/*/{Protheus.doc} InfTable
@description Formul�rio para informar a tabela de dados.
	
@author  Helitom Silva
@since   29/02/2016
@version 1.0
		
@return cRet, Caracter, Tabela de dados.

/*/
Method InfTable() Class IdaTBC

    Local cRet := ''

	/* Declara��o de cVariable dos componentes */
    Private cMGetInfo := 'Informe a tabela que ser� usada para o Cadastro de Tabelas de Customiza��o'
    Private cGetTab   := Space(3)

	/* Declara��o de Variaveis Private dos Objetos */
    SetPrvt("oDlgPerg", "oSayTab", "oGetTab", "oMGetInfo", "oSBtnOk")

	/* Definicao do Dialog e todos os seus componentes. */
    oDlgPerg  := MSDialog():New( 092, 222, 184, 560, "Cadastro de Tabelas de Customiza��o",,,.F.,,,,,,.T.,,,.T. )
    oMGetInfo := TMultiGet():New( 002, 002, {|u| If(PCount() > 0, cMGetInfo := u, cMGetInfo)}, oDlgPerg, 166, 028,,,CLR_BLACK,CLR_WHITE,,.T.,"",, {|| .F.},.F.,.F.,.F.,,,.F.,,  )
    oSayTab   := TSay():New( 036, 002, {|| "Tabela"}, oDlgPerg,,,.F.,.F.,.F.,.T., CLR_BLACK, CLR_WHITE, 020, 012)
    oGetTab   := TGet():New( 035, 027, {|u| If(PCount() > 0, cGetTab := u, cGetTab)}, oDlgPerg, 020, 008,'@!',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","",,)
    oSBtnOk   := SButton():New( 034, 142,1, {|| cRet := cGetTab, oDlgPerg:End()}, oDlgPerg,,"", )

    oDlgPerg:Activate(,,, .T.)

Return cRet


/*/{Protheus.doc} GetTbCAlias
@description Obtem Alias de Tabela Customizada

@author  Helitom Silva
@since   29/02/2016
@version 1.0

/*/
Method GetTbCAlias(p_cTabCus) Class IdaTBC

    Local cRet := ''
    Local cSQL := ''

    Default p_cTabCus := ''

    DbSelectArea(::GetTable())
    (::GetTable())->(DbGoTop())
    (::GetTable())->(DbSetOrder(1))

    If (::GetTable())->(DbSeek(FWxFilial(::GetTable()) + p_cTabCus))
        cRet := (::GetTable())->&(::GetTable() + '_TABREA')
    EndIf

Return cRet


/*/{Protheus.doc} GetTbCField
@description Obtem Nome de Campo de Tabela Customizada

@author  Helitom Silva
@since   29/02/2016
@version 1.0

/*/
Method GetTbCField(p_cTabCus, p_cField) Class IdaTBC

    Local cRet := ''

    Default p_cTabCus := ''
    Default p_cField  := ''

    cRet := ::GetTbCAlias(p_cTabCus) + '_' + AllTrim(p_cField)

Return cRet


/*/{Protheus.doc} GetTbCField
@description Obtem Nome de Campo de Tabela Customizada

@author  Helitom Silva
@since   29/02/2016
@version 1.0

/*/
Method PutTbCAlias(p_cTabCus, p_cDesc, p_cTabRea) Class IdaTBC

    Local lRet		  := .T.
    Local lExist 	  := .F.
    Local lInclui 	  := .T.

    Default p_cTabCus := ''
    Default p_cDesc   := ''
    Default p_cTabRea := ''

    DbSelectArea(::GetTable())
    (::GetTable())->(DbSetOrder(2))
    (::GetTable())->(DbGoTop())

    If (::GetTable())->(DbSeek(FWxFilial(::GetTable()) + p_cTabRea)) .and. &(::GetTable() + '_TABCUS') == p_cTabCus

        lInclui := .F.

    ElseIf (::GetTable())->(DbSeek(FWxFilial(::GetTable()) + p_cTabRea)) .and. .not. &(::GetTable() + '_TABCUS') == p_cTabCus
        Aviso("Aten��o", "A tabela real ja esta sendo usada por outro Alias Customizado!", {"OK"})
        lRet := .F.
    EndIf

    If lRet

        RecLock(::GetTable(), lInclui)

        &(::GetTable() + '_FILIAL') := xFilial(::GetTable())
        &(::GetTable() + '_TABCUS') := p_cTabCus
        &(::GetTable() + '_DESCRI') := p_cDesc
        &(::GetTable() + '_TABREA') := p_cTabRea

        (::GetTable())->(MsUnLock())

    EndIf

Return lRet