#INCLUDE 'PROTHEUS.CH'


/*/{Protheus.doc} IDAGPUPD
@description Retorna Grupos de Update da IdaTools
             
@author  Helitom Silva
@since   24/12/2013
@version 1.0		

/*/
User Function IDAGPUPD()

    Local aGrupos := {}

	/*
	  A cada item do Array de ser informado: {Grupo=Descri��o do Grupo}
	  Grupo � a fun��o que chama os compatibilizadores.
	  Exemplo: aGrupos := {'UPD001=Compatibilizadores do UPD001','UPD002=Compatibilizadores do UPD001','UPD002=Compatibilizadores do UPD001'}
	*/

    aGrupos := {'IDAUPD=IDATOOLS'}

Return aGrupos