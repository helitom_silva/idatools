#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICONN.CH"


/*/{Protheus.doc} IDAUPD
@description Retorna compatibilizadores do Grupo: IDAUPD - IdaTools.
 
@author	Helitom Silva
@since  18/06/2015

/*/
User Function IDAUPD()

    Local aData := {}

    aAdd(aData, {"IDAUPD01", "Impressora T�rmica", "18/06/2015" })
    aAdd(aData, {"IDAUPD02", "Tabelas de Customizacao", "29/02/2016" })
    aAdd(aData, {"IDAUPD03", "Cadastro de SQL Exportacao", "17/08/2016" })
    aAdd(aData, {"IDAUPD04", "Consulta Padr�o de Diretorio", "26/08/2016" })

Return aData