#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "IDATOOLS.CH"
#INCLUDE "IDAEXP.CH"

#DEFINE	CR Chr(13) + Chr(10)


/*/{Protheus.doc} IDAUPD03
@description Atualiza dicionario para a Customiza��o: Cobran�a e Negocia��o de Contas a Receber.

@author	 Helitom Silva
@version P11 e P10
@since	 29/02/2016

/*/
User Function IDAUPD03()

    Default oProcess  := Nil
    Default cEmpresa  := "01"

    oProcess:SetRegua2( 10 )

    oProcess:IncRegua2( 'Registrando Tabelas Customizadas')
    If !ExecTBC()
        Return
    EndIf

    oProcess:IncRegua2( 'Atualizando Perguntas')
    ExecSX1()

    oProcess:IncRegua2( 'Atualizando Tabelas Customizadas - Cabe�alho')
    ExecSX2Cust(cEmpresa)

    oProcess:IncRegua2( 'Atualizando Tabelas Customizadas - Campos')
    ExecSX3Cust()

    oProcess:IncRegua2( 'Atualizando Tabelas Padr�o - Cabe�alho')
    ExecSX2(cEmpresa)

    oProcess:IncRegua2( 'Atualizando Tabelas Padr�o - Campos')
    ExecSX3()

    oProcess:IncRegua2( 'Atualizando SX4')
    ExecSX4()

    oProcess:IncRegua2( 'Atualizando Tabelas Gen�ricas')
    ExecSX5()

    oProcess:IncRegua2( 'Atualizando Parametros')
    ExecSX6()

    oProcess:IncRegua2( 'Atualizando Gatilhos')
    ExecSX7()

    oProcess:IncRegua2( 'Atualizando Indices')
    ExecSIX()

    oProcess:IncRegua2( 'Atualizando Pastas de Tabelas')
    ExecSXA()

    oProcess:IncRegua2( 'Atualizando Consulta Padr�o')
    ExecSXB()

Return


/*/{Protheus.doc} ExecTBC
@description Cria relacionamento entre Alias customizado com Alias do Protheus

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015

/*/
Static Function ExecTBC()

    Local lRet := .F.

    lRet := Empresa1()

Return lRet


/*/{Protheus.doc} ExecSX1
@description Atualiza dicionario SX1 - Perguntas

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015

/*/
Static Function ExecSX1()
Return


/*/{Protheus.doc} ExecSX2Cust
@description Atualiza dicionario SX2 - Tabelas - Cabe�alho

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015

/*/
Static Function ExecSX2Cust(cEmpresa)

    SX2 := UPDSX2():CREATE(CEMPRESA)

    SX2:ADD(TBZ11, 'CADASTRO DE SQL EXPORTA��O')
    SX2:S('NOMESPA', 'CADASTRO DE SQL EXPORTA��O')
    SX2:S('NOMEENG', 'CADASTRO DE SQL EXPORTA��O')
    SX2:S('MODO'   , 'E')
    SX2:S('MODOUN' , 'E')
    SX2:S('MODOEMP', 'E')

    SX2:CONFIRM()

Return


/*/{Protheus.doc} ExecSX3Cust
@description Atualiza dicionario SX3 - Tabelas - Campos

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015

/*/
Static Function ExecSX3Cust()

    SX3 := UPDSX3():CREATE()

	/* TBZ11 - CADASTRO DE SQL EXPORTA��O */

    SX3:REMOVEALL(TBZ11, .F.)

    SX3:ADDFILIAL(TBZ11)

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11CODIGO)
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 6 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Codigo" )
    SX3:S("DESCRIC"	, "Codigo da Exporta��o" )
    SX3:S("RELACAO"	, "GetSXENum('" + TBZ11 + "', '" + Z11CODIGO + "')" )
    SX3:S("VISUAL"	, "V" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11GRUPO )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 12 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Grupo" )
    SX3:S("DESCRIC"	, "Grupo de Exporta��o" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11DESC )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 100 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Descri��o" )
    SX3:S("DESCRIC"	, "Descri��o da Exporta��o" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11SQL )
    SX3:S("TIPO"	, "M" )
    SX3:S("TAMANHO"	, 80 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "SQL" )
    SX3:S("DESCRIC"	, "SQL Exporta��o" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11DIR )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 250 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Gravar em..." )
    SX3:S("DESCRIC"	, "Diretorio p/ Exporta��o" )
    SX3:S("F3"		, "IDACDIR" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11FILE )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 30 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Arquivo" )
    SX3:S("DESCRIC"	, "Nome do Arquivo Exporta��o" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11TPFILE )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 1 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Tipo Arquivo" )
    SX3:S("DESCRIC"	, "Tipo do Arquivo Exporta��o" )
    SX3:S("CBOX"	, "C=CSV;X=XML" )
    SX3:S("RELACAO"	, "'C'" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11DELCSV )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 1 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Delimitador CSV" )
    SX3:S("DESCRIC"	, "Delimitador CSV" )
    SX3:S("RELACAO"	, "'�'" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11TFIELD )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 1 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Tit. Colunas" )
    SX3:S("DESCRIC"	, "T�tulo das Colunas" )
    SX3:S("CBOX"	, "D=Descri��o;C=Campo" )
    SX3:S("RELACAO"	, "'C'" )
    SX3:SETOBR()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11HREXP )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 8 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "99:99:99" )
    SX3:S("TITULO"	, "Hora" )
    SX3:S("DESCRIC"	, "Hora para Exporta��o" )
    SX3:SETOPCIONAL()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11DTEXP )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 10 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@R 99/99/9999" )
    SX3:S("TITULO"	, "Ult. Exporta��o" )
    SX3:S("DESCRIC"	, "Data da Exporta��o" )
    SX3:S("VISUAL"	, "V" )
    SX3:SETOPCIONAL()

    SX3:ADD(TBZ11)
    SX3:S("CAMPO"	, Z11MSBLQL )
    SX3:S("TIPO"	, "C" )
    SX3:S("TAMANHO"	, 1 )
    SX3:S("DECIMAL"	, 0 )
    SX3:S("PICTURE"	, "@!" )
    SX3:S("TITULO"	, "Bloqueado?" )
    SX3:S("DESCRIC"	, "Bloqueado?" )
    SX3:S("CBOX"	, "1=Sim;2=N�o" )
    SX3:S("RELACAO"	, "'2'" )
    SX3:S("VLDUSER"	, "Pertence('12')" )
    SX3:SETOBR()

    SX3:CONFIRM()

Return


/*/{Protheus.doc} ExecSX2
@description Atualiza dicionario SX2 - Tabelas - Cabe�alho

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015

/*/
Static Function ExecSX2(cEmpresa)
Return


/*/{Protheus.doc} ExecSX3
@description Atualiza dicionario SX3 - Tabelas - Campos

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015

/*/
Static Function ExecSX3()
Return


/*/{Protheus.doc} ExecSX4
@description Atualiza dicionario SX4

@author	Helitom Silva
@since	29/09/2015
 
/*/
Static Function ExecSX4()
Return


/*/{Protheus.doc} ExecSX5
@description Atualiza dicionario SX5 - Tabelas Genericas

@author	Helitom Silva
@since	29/09/2015

/*/
Static Function ExecSX5()
Return


/*/{Protheus.doc} ExecSX6
@description Atualiza dicionario SX6 - Tabelas

@author	Helitom Silva
@since	29/09/2015

/*/
Static Function ExecSX6()
Return


/*/{Protheus.doc} ExecSX7
@description Atualiza dicionario SX7 - Gatilhos

@author	Helitom Silva
@since	29/09/2015
 
/*/
Static Function ExecSX7()
Return


/*/{Protheus.doc} ExecSIX
@description Atualiza dicionario SIX - Indices

@author	Helitom Silva
@since	29/09/2015

/*/
Static Function ExecSIX()

    SIX := UPDSIX():CREATE()

    SIX:REMOVE(TBZ11, , .T.)
    SIX:ADD( TBZ11, Z11FILIAL + '+' + Z11CODIGO, 'Codigo Exporta��o', Z11CODIGO )
    SIX:ADD( TBZ11, Z11FILIAL + '+' + Z11GRUPO, 'Grupo', Z11GRUPO )
    SIX:ADD( TBZ11, Z11FILIAL + '+' + Z11DESC, 'Descri��o', Z11DESC )

    SIX:CONFIRM()

Return


/*/{Protheus.doc} ExecSXA
@description Atualiza dicionario SXA - Pastas de Tabelas

@author	Helitom Silva
@since	29/09/2015
 
/*/
Static Function ExecSXA()
Return


/*/{Protheus.doc} ExecSXB
@description Atualiza dicionario SXB - Consulta Padr�o

@author	Helitom Silva
@since	29/09/2015

/*/
Static Function ExecSXB()

    SXB := UPDSXB():CREATE()

    SXB:REMOVE( TBZ11 )

    SXB:ADDCONSULT(TBZ11, 'Operador de Cobran�a', , , TBZ11 )
    SXB:ADDINDICE( TBZ11, '01', '01', 'Filial + Codigo'	)
    SXB:ADDCOLUNA( TBZ11, '01', '01', 'Filial', , , Z11FILIAL )
    SXB:ADDCOLUNA( TBZ11, '01', '02', 'Grupo', , , Z11GRUPO )
    SXB:ADDCOLUNA( TBZ11, '01', '03', 'Codigo', , , Z11CODIGO )
    SXB:ADDCOLUNA( TBZ11, '01', '04', 'Descri��o', , , Z11DESC )
    SXB:ADDINDICE( TBZ11, '02', '02', 'Filial + Grupo'	)
    SXB:ADDCOLUNA( TBZ11, '02', '01', 'Filial', , , Z11FILIAL )
    SXB:ADDCOLUNA( TBZ11, '02', '02', 'Grupo', , , Z11GRUPO )
    SXB:ADDCOLUNA( TBZ11, '02', '03', 'Codigo', , , Z11CODIGO )
    SXB:ADDCOLUNA( TBZ11, '02', '04', 'Descri��o', , , Z11DESC )
    SXB:ADDINDICE( TBZ11, '03', '02', 'Filial + Descri��o'	)
    SXB:ADDCOLUNA( TBZ11, '03', '01', 'Filial', , , Z11FILIAL )
    SXB:ADDCOLUNA( TBZ11, '03', '02', 'Descri��o', , , Z11DESC )
    SXB:ADDCOLUNA( TBZ11, '03', '03', 'Codigo', , , Z11CODIGO )
    SXB:ADDCOLUNA( TBZ11, '03', '04', 'Grupo', , , Z11GRUPO )
    SXB:ADDRETORN( TBZ11, '01', TBZ11 + '->' + Z11CODIGO )

    SXB:CONFIRM()

Return


/*/{Protheus.doc} Empresa1
@description Cria relacionamento entre Alias customizado com Alias do Protheus na Empresa 1

@author	 Helitom Silva
@version P11 e P10
@since	 29/09/2015
/*/
Static Function Empresa1()

    Local lRet	   := .T.
    Local oTabCust := IdaTBC():New()

    lRet := Iif(lRet, oTabCust:PutTbCAlias('TBZ11', 'CADASTRO DE SQL EXPORT', 'ZQA'), .F.)

Return lRet