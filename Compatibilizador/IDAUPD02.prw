#INCLUDE 'PROTHEUS.CH'


/*/{Protheus.doc} IDAUPD02
@description Cadastro de Tabelas de Customização.
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0		

@obs O Nome da Tabela vem do parametro IDA_CADTBC

/*/
User Function IDAUPD02()

    Local oTabCust	  := IdaTBC():New()

    Private cAliasTBC := oTabCust:GetTable()

    Default oProcess  := Nil
    Default cEmpresa  := "XX"

    oProcess:SetRegua2( 4 )

    oProcess:IncRegua2( 'Atualizando SX2' )
    ExecSX2()

    oProcess:IncRegua2( 'Atualizando SX3' )
    ExecSX3()

    oProcess:IncRegua2( 'Atualizando SIX' )
    ExecSIX()

Return


/*/{Protheus.doc} ExecSX2
@description Atualiza dicionario SX2 - Tabelas

@author  Helitom Silva
@since   18/06/2015

/*/
Static Function ExecSX2()

    SX2 := UPDSX2():CREATE()

    SX2:ADD( cAliasTBC, "Tabelas de Customização", cAliasTBC + "_FILIAL+" + cAliasTBC + "_TABCUS" )

    SX2:CONFIRM()

Return


/*/{Protheus.doc} ExecSX3
@description Atualiza dicionario SX3 - Campos das Tabelas

@author  Helitom Silva
@since   23/09/2013

/*/
Static Function ExecSX3()

    SX3 := UPDSX3():CREATE()

    SX3:ADDFILIAL(cAliasTBC)

    SX3:ADD(cAliasTBC)
    SX3:S('CAMPO', 		cAliasTBC + '_TABCUS')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	5)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Alias Cust.')
    SX3:S('DESCRIC', 	'Alias Customizado')
    SX3:SETOBR()

    SX3:ADD(cAliasTBC)
    SX3:S('CAMPO', 		cAliasTBC + '_DESCRI')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	40)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Desc. Alias')
    SX3:S('DESCRIC', 	'Descrição do Alias')
    SX3:SETOPCIONAL()

    SX3:ADD(cAliasTBC)
    SX3:S('CAMPO', 		cAliasTBC + '_TABREA')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	3)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Alias Real')
    SX3:S('DESCRIC', 	'Alias Real')
    SX3:SETOBR()

    SX3:CONFIRM()

Return


/*/{Protheus.doc} ExecSIX
@description Atualiza dicionario SIX -  Indices

@author  Helitom Silva
@since   23/09/2013

/*/
Static Function ExecSIX()

    SIX := UPDSIX():CREATE()

    SIX:ADD( cAliasTBC, cAliasTBC + '_FILIAL+' + cAliasTBC + '_TABCUS', 'Alias Cust.', 'TABCUS' )
    SIX:ADD( cAliasTBC, cAliasTBC + '_FILIAL+' + cAliasTBC + '_TABREA', 'Alias Real', 'TABREA' )

    SIX:CONFIRM()

Return