#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "IDATOOLS.CH"
#INCLUDE "IDAEXP.CH"

#DEFINE	CR Chr(13) + Chr(10)


/*/{Protheus.doc} IDAUPD04
@description Atualiza dicionario para a Consulta Padr�o de Diretorio

@author	 Helitom Silva
@version P11 e P10
@since	 29/02/2016

/*/
User Function IDAUPD04()

    Default oProcess  := Nil
    Default cEmpresa  := "01"

    oProcess:SetRegua2( 10 )

    oProcess:IncRegua2( 'Atualizando Consulta Padr�o')
    ExecSXB()

Return


/*/{Protheus.doc} ExecSXB
@description Atualiza dicionario SXB - Consulta Padr�o

@author	Helitom Silva
@since	29/09/2015

/*/
Static Function ExecSXB()

    SXB := UPDSXB():CREATE()

    SXB:ADDCONSULT('IDACDIR', 'Diretorio', , , '.')
    SXB:S('COLUNA', 'RE')
    SXB:ADDINDICE('IDACDIR', '01', '01', 'Diretorio')
    SXB:S('CONTEM', 'U_IDACDIR()')
    SXB:ADDRETORN('IDACDIR', '01', '__cIDACDIR')

    SXB:CONFIRM()

Return