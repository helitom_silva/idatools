#INCLUDE 'PROTHEUS.CH'


/*/{Protheus.doc} IDAUPD01
@description Cadastro de Esta��o.
	
@author  Helitom Silva
@since   18/06/2015
@version 1.0		

@obs O Nome da Tabela vem do parametro IDA_CADEST

/*/
User Function IDAUPD01()

    Local oEstImp	  := IdaCEI():New()

    Private cAliasCEI := oEstImp:GetTable()

    Default oProcess  := Nil
    Default cEmpresa  := "XX"

    oProcess:SetRegua2( 4 )

    oProcess:IncRegua2( 'Atualizando SX2' )
    ExecSX2()

    oProcess:IncRegua2( 'Atualizando SX3' )
    ExecSX3()

    oProcess:IncRegua2( 'Atualizando SIX' )
    ExecSIX()

    oProcess:IncRegua2( 'Atualizando SXB' )
    ExecSXB()

Return


/*/{Protheus.doc} ExecSX2
@description Atualiza dicionario SX2 - Tabelas

@author  Helitom Silva
@since   18/06/2015

/*/
Static Function ExecSX2()

    SX2 := UPDSX2():CREATE()

    SX2:ADD( cAliasCEI, "Esta��o Trabalho", cAliasCEI + "_FILIAL+" + cAliasCEI + "_CODIGO" )

    SX2:CONFIRM()

Return


/*/{Protheus.doc} ExecSX3
@description Atualiza dicionario SX3 - Campos das Tabelas

@author  Helitom Silva
@since   23/09/2013

/*/
Static Function ExecSX3()

    SX3 := UPDSX3():CREATE()

    SX3:ADDFILIAL(cAliasCEI)

    SX3:ADD(cAliasCEI)
    SX3:S('CAMPO', 		cAliasCEI + '_CODIGO')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	15)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Ip Esta��o')
    SX3:S('DESCRIC', 	'Ip da Esta��o')
    SX3:S('RELACAO', 	'GetClientIP()')
    SX3:S('VISUAL', 	'V')
    SX3:SETCHAVE()

    SX3:ADD(cAliasCEI)
    SX3:S('CAMPO', 		cAliasCEI + '_IMP1')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	30)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Imp Laser')
    SX3:S('DESCRIC', 	'Impressora Laser')
    SX3:SETOPCIONAL()

    SX3:ADD(cAliasCEI)
    SX3:S('CAMPO', 		cAliasCEI + '_IMP2')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	30)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Imp Termica')
    SX3:S('DESCRIC', 	'Impressora Termica')
    SX3:SETOPCIONAL()

    SX3:ADD(cAliasCEI)
    SX3:S('CAMPO', 		cAliasCEI + '_PSQSTF')
    SX3:S('TIPO', 		'C')
    SX3:S('TAMANHO',	1)
    SX3:S('DECIMAL', 	0)
    SX3:S('PICTURE',	'@!')
    SX3:S('TITULO', 	'Exibe Setup?')
    SX3:S('DESCRIC', 	'Exibe Setup?')
    SX3:S('RELACAO', 	'"2"')
    SX3:S('CBOX', 		'1=Nao;2=Sim')
    SX3:SETOPCIONAL()

    SX3:CONFIRM()

Return


/*/{Protheus.doc} ExecSIX
@description Atualiza dicionario SIX -  Indices

@author  Helitom Silva
@since   23/09/2013

/*/
Static Function ExecSIX()

    SIX := UPDSIX():CREATE()

    SIX:ADD( cAliasCEI, cAliasCEI + '_FILIAL+' + cAliasCEI + '_CODIGO', 'Codigo', 'CHAVE' )
    SIX:ADD( cAliasCEI, cAliasCEI + '_FILIAL+' + cAliasCEI + '_CODIGO+' + cAliasCEI + '_IMP1', 'Impressora', 'IMPRE' )

    SIX:CONFIRM()

Return