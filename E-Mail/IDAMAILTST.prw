#INCLUDE 'PROTHEUS.CH'

#DEFINE	CR Chr(13) + Chr(10)

User Function IDAMAILTST()

    cMailDest := 'helitom@idainformatica.com'
    cMensagem := 'IDAMAILTST - Mensagem'
    cAssunto  := 'IDAMAILTST - Assunto'
    cError    := ''
    cMailInfo := ''

    //U_IDAMAIL(cMailDest, cAssunto, cMensagem, @cError, @cMailInfo)
    FSSendMail( cMailDest, cAssunto, cMensagem, /*cAttach*/ )

    If .not. Empty(AllTrim(cError))
        MsgYesNo(cUserName + CR + 'Ocorreu erro ao Enviar E-Mail' + CR + cError + CR + cMailInfo, 'Erro Envio E-Mail')
    EndIf

Return